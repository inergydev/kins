<?php
  include_once ("../php/define.php");
  include_once ("../php/logger.php");
  include_once ("../php/db_util.php");


  function mssql_escape_string($data) {
    if ( !isset($data) or empty($data) ) return '';
    if ( is_numeric($data) ) return $data;

    $non_displayables = array(
        '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
        '/%1[0-9a-f]/',             // url encoded 16-31
        '/[\x00-\x08]/',            // 00-08
        '/\x0b/',                   // 11
        '/\x0c/',                   // 12
        '/[\x0e-\x1f]/'             // 14-31
    );
    foreach ( $non_displayables as $regex )
        $data = preg_replace( $regex, '', $data );
    $data = str_replace("'", "''", $data );
    return $data;
  }

  $xss = "<script>alert('XSS공격')</script>";

  $uid = 301;
  $title = "공지사항";
  $contents = "공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항"; 
  $attachments = ""; 
  $sql = "INSERT INTO KINSDB.dbo.tb_noti (uid, title, contents, attachments) VALUES (";
  $sql .= "".$uid.", "."'".$title.$i."', "."'".$contents."', "."'".$attachments."'";
  $sql .= ")";

  $html = "<html lang='ko'>";
  $html = "<head>";
  // $html .= $xss;
  $html .= "</head>";
  $html .= "<body>";
  $html .= "test"."<br>";
  $html .= htmlspecialchars($xss)."<br>";
  $html .= htmlentities($xss)."<br>";
  $html .= strip_tags($xss)."<br>";
  $html .= "</body>";
  $html .= "</html>";
  echo ($html);

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    debug("DB Connection Faild.");
    return;
  }

  // Query
  // $uid = 301;
  // $title = "공지사항";
  // $contents = "공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항공지사항"; 
  // $attachments = ""; 
  
  // for ($i=1; $i<=100; $i++) {
  //   $sql = "INSERT INTO KINSDB.dbo.tb_noti (uid, title, contents, attachments) VALUES (";
  //   $sql .= "".$uid.", "."'".$title.$i."', "."'".$contents."', "."'".$attachments."'";
  //   $sql .= ")";
  //   $result = sqlsrv_query( $conn, $sql );
  //   debug($result);
  // }
  
  // // Query
  // $org_name = "일반사용자";
  // $contact_name = "일반사용자";
  // $rank = "일반사용자"; 
  // $id = "user"; 
  // $pwd = "qwer1234"; 
  // $user_auth = "USER"; 
  
  // for ($i=1; $i<=100; $i++) {
  //   $sql = "INSERT INTO KINSDB.dbo.tb_user (org_name, contact_name, rank, id, pwd, user_auth) VALUES (";
  //   $sql .= "'".$org_name."', "."'".$contact_name."', "."'".$rank."', "."'".$id.$i."', "."'".$pwd."', "."'".$user_auth."'";
  //   $sql .= ")";
  //   $result = sqlsrv_query( $conn, $sql );
  //   debug($result);
  // }


  // DB Disconnection
  dbDisconnect($conn, $result);
?>