$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);

  if(userInfo.USER_AUTH === "USER") {
    $(location).attr("href", "../error.html");
  }

  if (userInfo.USER_AUTH === "ADMIN") {
    $("#searchType").children("[value='ALL']").remove();
    $("#searchType").children("[value='ADMIN']").remove();
  }

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);
  if (pagePrevUrl === "member_mod") fnPrevDataSet();
  else if (pagePrevUrl === "member_reg") {
    let isPrevDataSync = localStorage.getItem("member_reg");
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("member_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    let searchOptionChecked = false;
    let searchKeywordLength = $("#searchKeyword").val().length;
    if ($("#listOrgName").is(":checked")) searchOptionChecked = true;
    if ($("#listContactName").is(":checked")) searchOptionChecked = true;
    if ($("#listId").is(":checked")) searchOptionChecked = true;

    fnGetData(1);
  });

  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("member_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)
    $("#searchType").val(obj.searchType)

    $("#listOrgName").prop("checked", false);
    $("#listContactName").prop("checked", false);
    $("#listId").prop("checked", false);

    for (let i = 0; i < obj.searchOptions.length; i++) {
      let aData = obj.searchOptions[i];
      if (aData === "org_name") $("#listOrgName").prop("checked", true);
      if (aData === "contact_name") $("#listContactName").prop("checked", true);
      if (aData === "id") $("#listId").prop("checked", true);
    }
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();
  let option_1 = $("#listOrgName").is(":checked");
  let option_2 = $("#listContactName").is(":checked");
  let option_3 = $("#listId").is(":checked");

  if (!option_1 && !option_2 && !option_3) {
    alert("검색옵션을 1개 이상 선택해주세요");
    return false;
  }

  if ((option_1 || option_2 || option_3) && searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  let searchOptions = new Array();
  if ($("#listOrgName").is(":checked")) searchOptions.push("org_name");
  if ($("#listContactName").is(":checked")) searchOptions.push("contact_name");
  if ($("#listId").is(":checked")) searchOptions.push("id");

  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val(),
    searchType : $("#searchType").val(),
    searchOptions : searchOptions
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/member_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      fnVisibleElement("container", true);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnCurrStateSave(obj) {
  localStorage.setItem("member_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr>";
      buildHtml = buildHtml + "<td><input type='checkbox' class='chk_info' id='chk_info" + aData.uid + "' value='" + aData.uid + "'><label for='chk_info" + aData.uid + "'><span class='blind'>선택</span></label></td>";
      buildHtml = buildHtml + "<td>" + aData.no + "</td>";
      buildHtml = buildHtml + "<td>" + aData.orgName + "</td>";
      buildHtml = buildHtml + "<td>" + aData.contactName + " " + aData.rank + "</td>";
      buildHtml = buildHtml + "<td>" + aData.id + "</td>";

      if (aData.lastAccessDate === null) {
        buildHtml = buildHtml + "<td>-</td>";
      } else {
        buildHtml = buildHtml + "<td>" + dateFormat("full", aData.lastAccessDate.date) + "</td>";
      }
      buildHtml = buildHtml + "<td><button type='button' class='btn_mod' value='" + aData.uid + "'>수정</button></td>";
      buildHtml = buildHtml + "</tr>";

      $("#ownTable > tbody:last").append(buildHtml);
    // });
    }

    // selectbox click event setting
    $("#chk_all").click(function(e){      
      let chkInfos = $(".chk_info:checkbox").toArray();
      for (let i = 0; i < chkInfos.length; i++) {
        let aData = chkInfos[i];
        $(aData).prop("checked", $(this).is(":checked"));
      }
    });

    $(".chk_info").click(function(e){
      let totalCount = $(".chk_info:checkbox").toArray().length;
      let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

      console.log(totalCount + " " + checkedCount);

      let chkAllCheckd = false;
      chkAllCheckd = (totalCount === checkedCount) ? true : false;
      $("#chk_all").prop("checked", chkAllCheckd);
    });


    $(".btn_mod").click(function(e){
      e.preventDefault();
      localStorage.setItem("member_mod", $(this).val());
      $(location).attr("href", "./member_mod.html");
    });

    $("a.btn_del").unbind("click");
    $("a.btn_del").click(function(e){
      e.preventDefault();
      let checkedData = $(".chk_info:checkbox:checked").toArray();
      if (checkedData.length === 0) {
        alert("삭제할 데이터를 선택해주세요");
        return;
      }

      if (confirm("삭제하시겠습니까?")) {
        let delArr = new Array();
        for (let i = 0; i < checkedData.length; i++) {
          let aData = checkedData[i];
          delArr.push(Number($(aData).val()));
        }
        
        console.log(delArr);
  
        let reqObj = {
          action : "delete",
          delArr : delArr
        };
    
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/member_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            if(result.errCode === 0) {
              let movePage = pager.currPage;
  
              if (pager.currPage === 1) movePage = 1;
              else {
                if (delArr.length === pager.defPagerPageSize) {
                  movePage = pager.currPage - 1;
                }
              }
              fnGetData(movePage);
            } else {
              alert(result.errMsg);
            }
          }
        });
      }
    });

    // Draw Pager
    fnDrawPager(data, fnGetData);

  } else {
    $("#ownTable > tbody:last").append("<tr><td colspan='7'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}
