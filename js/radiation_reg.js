$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  $("#regBtn").click(function(e){
    fnRegData(userInfo);
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("rad_reg", false);
    $(location).attr("href", "./radiation_list.html");
  });

  $("#modAccidentYear").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAccidentMonth").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAccidentDay").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
  
  $("#modCountry").keyup(function(e){
    $(this).val($(this).val().replace(/[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi,""));
    $(this).val($(this).val().replace(/[0-9]/g,""));
  });

  $("#modAccidentLoc").keyup(function(e){
    $(this).val($(this).val().replace(/[\{\}\[\]\/?.,;:|\)*~`!^\-_+<>@\#$%&\\\=\(\'\"]/gi,""));
    $(this).val($(this).val().replace(/[0-9]/g,""));
  });
  
}

function fnRegData(userInfo) {
  if(!fnValidationCheck()) return;
  let dateString = $("#modAccidentYear").val() + "-" + pad($("#modAccidentMonth").val(), 2) + "-" +  pad($("#modAccidentDay").val(), 2);

  if(confirm("등록하시겠습니까?")) {
    let reqObj = {
      action : "insert",
      uid : userInfo.UID,
      country : $("#modCountry").val(),
      loc : $("#modAccidentLoc").val(),
      facility : $("#modAccidentFacility").val(),
      date : dateString,
      level : $("#modAccidentLevel").val(),
      kinsLevel : $("#modAccidentKinsLevel").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/radiation_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          localStorage.setItem("rad_reg", true);
          $(location).attr("href", "./radiation_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modCountry").val().length >= 30) {
    alert("국가는 30자 이내여야 합니다");
    return false;
  } else if ($("#modCountry").val().length === 0) {
    alert("국가를 입력해주세요");
    return false;
  }
  if ($("#modAccidentLoc").val().length >= 30) {
    alert("사고 위치는 30자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentLoc").val().length === 0) {
    alert("사고 위치를 입력해주세요");
    return false;
  }
  if ($("#modAccidentFacility").val().length >= 30) {
    alert("사고 시설은 30자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentFacility").val().length === 0) {
    alert("사고 시설을 입력해주세요");
    return false;
  }
  let dateString = $("#modAccidentYear").val() + pad($("#modAccidentMonth").val(), 2) + pad($("#modAccidentDay").val(), 2);
  if (!fnDateCheck(dateString)) {
    alert("사고 일시의 날짜형식이 올바르지 않습니다.");
    return false;
  }
  if ($("#modAccidentLevel").val().length >= 30) {
    alert("사고 등급은 5자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentLevel").val().length === 0) {
    alert("사고 등급을 입력해주세요");
    return false;
  }
  if ($("#modAccidentKinsLevel").val().length >= 30) {
    alert("원자력 안전위원회 위기경보등급은 10자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentKinsLevel").val().length === 0) {
    alert("원자력 안전위원회 위기경보등급을 입력해주세요");
    return false;
  }
  return true;
}