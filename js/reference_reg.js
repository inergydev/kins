$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  objUserInfo = userInfo;

  $("#attachFile").attr("accept", gFileUploadAllowExtention);

  $("#attachRegBtn").click(function(e){
    $("#attachFile").click();
  });

  $("#attachFile").change(function(e){

    let file = $("#attachFile").prop("files")[0];
    let fileName = file.name;

    $("#attachPreview").html("");
    $("#attachPreview").html(fileName + " <button class='del_file' id='attachDelBtn'><span class='blind'>파일삭제</span></button>");

    $("#attachDelBtn").click(function(e){
      $("#attachPreview").html("");
      $('#attachFile').val("");
    });
  });

  $("#regBtn").click(function(e){
    fnRegData();
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("ref_reg", false);
    $(location).attr("href", "./reference_list.html");
  });
}

function fnRegData() {
  if(!fnValidationCheck()) return;
  
  let formData = new FormData();
  formData.append("uid", objUserInfo.UID);
  formData.append("action", "insert");
  formData.append("title", $("#modTitle").val());
  formData.append("content", "");

  let file = $("#attachFile").prop("files")[0];
  if (file !== undefined) {
    formData.append("file", $("#attachFile")[0].files[0]);
  }

  $.ajax({
    type: "POST",
    contentType: "multipart/form-data",
    url: "../php/reference_api.php",
    cache: false,
    processData: false,
    dataType: 'json',
    mimeType: 'multipart/form-data',
    contentType: false,
    processData: false,
    data: formData,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if(!alert("등록되었습니다")) {
          localStorage.setItem("ref_reg", true);
          $(location).attr("href", "./reference_list.html");
        }
      } else if (result.errCode === 2) {
        alert("파일업로드에 실패하였습니다");
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnValidationCheck() {
  if ($("#modTitle").val().length >= 100) {
    alert("제목은 100자 이내여야 합니다");
    return false;
  } else if ($("#modTitle").val().length === 0) {
    alert("제목을 입력해주세요");
    return false;
  }

  let file = $("#attachFile").prop("files")[0];
  if (file === undefined) {
    alert("첨부파일을 등록해주세요");
    return false;
  }
  
  return true;
}