$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  $("#regBtn").click(function(e){
    fnRegData(userInfo);
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("obj_reg", false);
    $(location).attr("href", "./object_list.html");
  });

  $("#modEntryYear").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryMonth").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryDay").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
  
  $("#modEntryHour").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryMinute").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAge").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modSex").append("<option value='' selected disabled>선택</option>");
  let types = fnGetCode("objSex");
  for (let i = 0; i < types.length; i++) {
    let aData = types[i];
    let html = "<option value='" + aData + "'>" + aData + "</option>";
    $("#modSex").append(html);
  }
}

function fnRegData(userInfo) {
  if(!fnValidationCheck()) return;
  let dateString = $("#modEntryYear").val() + "-" + pad($("#modEntryMonth").val(), 2) + "-" +  pad($("#modEntryDay").val(), 2) +  "-" + pad($("#modEntryHour").val(), 2) +  "-" + pad($("#modEntryMinute").val(), 2);

  console.log(dateString);

  if(confirm("등록하시겠습니까?")) {
    let reqObj = {
      action : "insert",
      uid : userInfo.UID,
      name : $("#modName").val(),
      country : $("#modCountry").val(),
      age : Number($("#modAge").val()),
      sex : $("#modSex").val(),
      date : dateString,
      vehicleInfo : $("#modVehicleInfo").val(),
      departurePlace : $("#modDeparturePlace").val(),
      contaminantDetail : $("#modContaminantDetail").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/object_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          localStorage.setItem("obj_reg", true);
          $(location).attr("href", "./object_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modName").val().length >= 20) {
    alert("이름은 20자 이내여야 합니다");
    return false;
  } else if ($("#modName").val().length === 0) {
    alert("이름를 입력해주세요");
    return false;
  }

  if ($("#modCountry").val().length >= 20) {
    alert("국적은 20자 이내여야 합니다");
    return false;
  } else if ($("#modCountry").val().length === 0) {
    alert("국적을 입력해주세요");
    return false;
  }

  if ($("#modAge").val().length === 0) {
    alert("나이를 입력해주세요");
    return false;
  }

  if ($("#modAge").val() === null) {
    alert("성별을 선택해주세요.");
    return false;
  }

  let dateString = $("#modEntryYear").val() + pad($("#modEntryMonth").val(), 2) + pad($("#modEntryDay").val(), 2);
  if (!fnDateCheck(dateString)) {
    alert("날짜의 형식이 올바르지 않습니다.");
    return false;
  }

  let hour = Number($("#modEntryHour").val());
  let minute = Number($("#modEntryMinute").val());
  
  if ($("#modEntryHour").val().length === 0) {
    alert("시간을 입력해주세요.");
    return false;
  }

  if (hour > 24) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modEntryMinute").val().length === 0) {
    alert("분을 입력해주세요.");
    return false;
  }

  if (minute > 59) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modVehicleInfo").val().length >= 30) {
    alert("여객정보는 30자 이내여야 합니다");
    return false;
  } else if ($("#modVehicleInfo").val().length === 0) {
    alert("여객정보를 입력해주세요");
    return false;
  }

  if ($("#modDeparturePlace").val().length >= 30) {
    alert("출발지는 30자 이내여야 합니다");
    return false;
  } else if ($("#modDeparturePlace").val().length === 0) {
    alert("출발지를 입력해주세요");
    return false;
  }

  if ($("#modContaminantDetail").val().length === 0) {
    alert("오염 상세정보입력해주세요");
    return false;
  }

  return true;
}