
gPagePrevUrl = "";
gPageCurrUrl = "";
gFileUploadAllowExtention = ".jpg, .jpeg, .gif, .bmp, .png, .zip, .txt, .hwp, .doc, .html, .xls, .xlsx, .ppt, .pdf";

$(document).ready(function(){
  gPagePrevUrl = localStorage.getItem("url");
  localStorage.setItem("url", window.location.href);
  gPageCurrUrl = localStorage.getItem("url");

  console.log(gPageCurrUrl);
  console.log(gPagePrevUrl);
});

// fnCheckSession OK -> fnDrawCommonView -> 
// Session Check
function fnCheckSession(actionFlag, redirectUrl, callbackFunction) {
  let reqObj = {type:"checkSession"};

  // API Call
  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/session_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (actionFlag) $(location).attr("href", redirectUrl);
        fnDrawCommonView(result.userInfoObj, callbackFunction);
      } else {
        if (!actionFlag) $(location).attr("href", redirectUrl);
      }
    }
  });
}

// Draw Common View
function fnDrawCommonView(userInfoObj, callbackFunction) {
  if ( $('.header_wrap').length ){
    $.get("header.html", function (data) {
      $(".header_wrap").html(data);
      $("#orgName").text(userInfoObj.ORG_NAME);
      $("#logout").click(function() {
        // Request Object Setting
        let reqObj = {};
        
        // API Call
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/logout_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            $(location).attr("href", "./login.html");
          }
        });
      });
      console.log("header");
    });
  }

  if ( $('.footer_wrap').length ){
    $.get("footer.html", function (data) {
      $(".footer_wrap").html(data);
      if (userInfoObj.USER_AUTH === "USER") {
        $("#adminMenu").hide();
      }

      $("#adminMenuBtn").click(function(e){
        e.preventDefault();
        localStorage.setItem("member_reg", true);
        localStorage.setItem("member_list", null);
    
        $(location).attr("href", "./member_list.html");
      });

      $("#noticeMenuBtn").click(function(e){
        e.preventDefault();
        localStorage.setItem("notice_view", true);
        localStorage.setItem("notice_list", null);
    
        $(location).attr("href", "./notice_list.html");
      });

      console.log("footer");
      callbackFunction(userInfoObj);
    });
  }
}

function fnVisibleElement(id, show) {
  if (show)  $("#" + id).show();
  else $("#" + id).hide();
}

function fnParseUrlFileName(url) {
  let cvtUrl = url.split('/').pop().split('#')[0].split('?')[0];
  cvtUrl = cvtUrl.replace(".html", "");
  return cvtUrl;
}

function fnGetParameter(paramName) {
  let returnValue;
  let url = location.href;
  let parameters = (url.slice(url.indexOf('?') + 1, url.length)).split('&');

  for (let i = 0; i < parameters.length; i++) {
    let varName = parameters[i].split('=')[0];
      if (varName.toUpperCase() == paramName.toUpperCase()) {
          returnValue = parameters[i].split('=')[1];
          return decodeURIComponent(returnValue);
      }
  }
};

// Draw Pager
function fnRemovePager() {
  // Remove Pager
  $("#pager").html("");
}

function fnDrawPager(data, fnGetData) {
  fnRemovePager();

  // draw Pager
  let pager = data.pager;
  let pagerBlockTotalCount = Math.ceil((pager.totalCount / pager.defPagerPageSize) / pager.defPagerBlockSize);
  let pagerPageToTalCount = Math.ceil((pager.totalCount / pager.defPagerPageSize));
  let pagerCurrentBlock = Math.ceil(pager.currPage / pager.defPagerBlockSize);
  let pagerLastPage = (pagerCurrentBlock * pager.defPagerBlockSize);
  let pagerFirstPage = (pagerLastPage - (pager.defPagerBlockSize - 1));

  let buildHtml = "";
  buildHtml = buildHtml + "<a href='javascript:void(0);' class='btn_first' id='pagerFirstPage'><sapn class='blind'>처음으로</span></a>";
  buildHtml = buildHtml + "<a href='javascript:void(0);' class='btn_prev' id='pagerPrevBlock'><sapn class='blind'>이전으로</span></a>";

  for (pagerFirstPage; pagerFirstPage <= pagerLastPage; pagerFirstPage++) {
    if (pagerFirstPage === pager.currPage) buildHtml = buildHtml + "<a href='javascript:void(0)' name='pagerPage' value='" + pagerFirstPage + "' class='on'>" + pagerFirstPage + "</a>";
    else buildHtml = buildHtml + "<a href='javascript:void(0)' name='pagerPage' value='" + pagerFirstPage + "'>" + pagerFirstPage + "</a>";
    if (pagerPageToTalCount <= pagerFirstPage) break;
  }

  buildHtml = buildHtml + "<a href='javascript:void(0);' class='btn_next' id='pagerNextBlock'><sapn class='blind'>다음으로</span></a>";
  buildHtml = buildHtml + "<a href='javascript:void(0);' class='btn_last' id='pagerLastPage'><sapn class='blind'>마지막으로</span></a>";

  $("#pager").append(buildHtml);

  $("a[name=pagerPage").click(function(e){
    e.preventDefault();
    let movePage = Number($(this).attr("value"));

    if (pager.currPage === movePage) return;
    else fnGetData(movePage);
  });
  
  $("#pagerFirstPage" ).unbind("click");
  $("#pagerFirstPage").click(function(e){
    e.preventDefault();
    if (pager.currPage === 1) return;
    else fnGetData(1);
  });

  $("#pagerPrevBlock" ).unbind("click");
  $("#pagerPrevBlock").click(function(e){
    e.preventDefault();
    if (pagerCurrentBlock === 1) return;
    else {
      let pagerLastPage = ((pagerCurrentBlock - 1) * pager.defPagerBlockSize);
      let pagerFirstPage = (pagerLastPage - (pager.defPagerBlockSize - 1));
      fnGetData(pagerFirstPage);
    }
  });

  $("#pagerNextBlock" ).unbind("click");
  $("#pagerNextBlock").click(function(e){
    e.preventDefault();
    if (pagerBlockTotalCount === 0 || pagerCurrentBlock === pagerBlockTotalCount) return;
    else {
      let pagerLastPage = ((pagerCurrentBlock + 1) * pager.defPagerBlockSize);
      let pagerFirstPage = (pagerLastPage - (pager.defPagerBlockSize - 1));
      fnGetData(pagerFirstPage);
    }
  });

  $("#pagerLastPage" ).unbind("click");
  $("#pagerLastPage").click(function(e){
    e.preventDefault();
    let lastPage = Math.ceil(pager.totalCount / pager.defPagerPageSize);

    if (lastPage === 0 || lastPage === pager.currPage) return;
    else fnGetData(lastPage);
  });
}

function dateFormat(type, dateString) {
  if (dateString === null || dateString.length <= 0) return "";

  // mssql datetime format convert(IE, Chrome)
  let strArr = dateString.split(".");
  let parseDateString = strArr[0].replace(/-/gi, "/");

  let date = new Date(parseDateString);
  let year = date.getFullYear();
  let month = pad(date.getMonth() + 1, 2);
  let day = pad(date.getDate(), 2);
  let hour = pad(date.getHours(), 2);
  let minutes = pad(date.getMinutes(), 2);
  let second = pad(date.getSeconds(), 2);

  if (type === "full") {
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes + ":" + second;
  } else if (type === "compact") {
    return year + "-" + month + "-" + day;
  } else if (type === "compactTime") {
    return year + "-" + month + "-" + day + " " + hour + ":" + minutes;
  }
}

function pad(n, width) {
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}

function fnDateInputFormat( e, oThis ){
  let num_arr = [ 
      97, 98, 99, 100, 101, 102, 103, 104, 105, 96,
      48, 49, 50, 51, 52, 53, 54, 55, 56, 57
  ]
  
  let key_code = ( e.which ) ? e.which : e.keyCode;

  console.log(key_code);

  if( num_arr.indexOf( Number( key_code ) ) != -1 ){
      let len = oThis.val().length;
      console.log(len);
      if( len == 4 ) oThis.val(oThis.val() + "-");
      if( len == 7 ) oThis.val(oThis.val() + "-");
  }
}

function fnDateCheck(dateString)
{
  if (dateString.length < 8) return false;

  let year = dateString.substring(0, 4);

  if (Number(year) < 1900) return false;
  if (Number(year) > 2100) return false;

  let month = dateString.substring(4, 6);
  let day = dateString.substring(6, 8);

  let date = new Date(year + "-" + month + "-" + day);

  console.log(date);

  if ( Object.prototype.toString.call(date) === "[object Date]" ) {
    if ( isNaN( date.getTime() ) ) return false;
    else return true;
  }
  else return false;
}

function fnGetCode(type) {
  // type :: resArea, resAirSea, resEquipName, insAirSea, objSex
  let codeObj = [];
  switch (type) {
    case "resArea" :
      codeObj.push({"인천광역시" : ["인천국제공항", "인천국제여객터미널"]}); 
      codeObj.push({"서울특별시" : ["김포국제공항"]}); 
      codeObj.push({"부산광역시" : ["김해국제공항", "부산국제여객터미널"]}); 
      codeObj.push({"제주도" : ["제주국제공항", "제주국제여객터미널"]}); 
      codeObj.push({"충청북도" : ["청주국제공항"]}); 
      codeObj.push({"대구광역시" : ["대구국제공항"]});
      codeObj.push({"전라남도" : ["무안국제공항", "광양국제여객터미널"]});  
      codeObj.push({"강원도" : ["양양국제공항", "동해국제여객터미널"]});  
      break;
    case "resEquipName" :
      codeObj.push("방사선 특정기_UltraRadiac"); 
      codeObj.push("방사선측정기_RadEYE PRD-ER"); 
      codeObj.push("표면 오염측정기_MCB2"); 
      codeObj.push("비상대응키트_Emergency kit(CANBERRA)"); 
      break;
    case "insAirSea" :
      codeObj.push("항공"); 
      codeObj.push("선박"); 
      break;
    case "objSex" :
      codeObj.push("남"); 
      codeObj.push("여"); 
      break;
    default :
      break;
  }

  return codeObj;
}