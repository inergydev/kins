$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);
  fnVisibleElement("container", true);
  objUserInfo = userInfo;

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);
  if (pagePrevUrl === "object_mod") fnPrevDataSet();
  else if (pagePrevUrl === "object_reg") {
    let isPrevDataSync = localStorage.getItem("obj_reg");
    console.log(isPrevDataSync);
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("obj_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });

  $("#listEntryDate").click(function(e){
    let checked = $("#listEntryDate").is(":checked");
    $("#listEntryStartDate").prop('disabled', !checked);
    $("#listEntryEndDate").prop('disabled', !checked);
  });

  $("#listEntryStartDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#listEntryEndDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("obj_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)
    $("#listEntryDate").prop("checked", false);
    $("#listName").prop("checked", false);
    $("#listCountry").prop("checked", false);
    $("#listVehicleInfo").prop("checked", false);
    $("#listDeparturePlace").prop("checked", false);

    if (obj.searchDateOption) {
      $("#listEntryDate").prop("checked", true);
      $("#listEntryStartDate").prop("disabled", false);
      $("#listEntryEndDate").prop("disabled", false);
      $("#listEntryStartDate").val(obj.dateStart.replace(/-/gi, ""));
      $("#listEntryEndDate").val(obj.dateEnd.replace(/-/gi, ""));
    }

    for (let i = 0; i < obj.searchOptions.length; i++) {
      let aData = obj.searchOptions[i];
      if (aData === "name") $("#listName").prop("checked", true);
      if (aData === "country") $("#listCountry").prop("checked", true);
      if (aData === "vehicle_info") $("#listVehicleInfo").prop("checked", true);
      if (aData === "departure_place") $("#listDeparturePlace").prop("checked", true);
    }
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();
  let option_1 = $("#listEntryDate").is(":checked");
  let option_2 = $("#listName").is(":checked");
  let option_3 = $("#listCountry").is(":checked");
  let option_4 = $("#listVehicleInfo").is(":checked");
  let option_5 = $("#listDeparturePlace").is(":checked");

  if (!option_1 && !option_2 && !option_3 && !option_4 && !option_5) {
    alert("검색옵션을 1개 이상 선택해주세요");
    return false;
  }

  if ((option_2 || option_3 || option_4 || option_5) && searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  if(!fnValidationCheck()) return;

  let searchOptions = new Array();
  if ($("#listName").is(":checked")) searchOptions.push("name");
  if ($("#listCountry").is(":checked")) searchOptions.push("country");
  if ($("#listVehicleInfo").is(":checked")) searchOptions.push("vehicle_info");
  if ($("#listDeparturePlace").is(":checked")) searchOptions.push("departure_place");

  let dateStartString = $("#listEntryStartDate").val();
  let dateEndString = $("#listEntryEndDate").val();
  let isSearchDateOption = false;
  if ($("#listEntryDate").is(":checked")) {
    isSearchDateOption = true;
    dateStartString = dateStartString.substring(0, 4) + "-" + dateStartString.substring(4, 6) + "-" + dateStartString.substring(6, 8);
    dateEndString = dateEndString.substring(0, 4) + "-" + dateEndString.substring(4, 6) + "-" + dateEndString.substring(6, 8);
  }

  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val(),
    searchOptions : searchOptions,
    searchDateOption : isSearchDateOption,
    dateStart : dateStartString,
    dateEnd : dateEndString,
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/object_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
        fnVisibleElement("container", true);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnValidationCheck() {
  if ($("#listEntryDate").is(":checked")) {
    let dateStartString = $("#listEntryStartDate").val();
    let dateEndString = $("#listEntryEndDate").val();

    if (!fnDateCheck(dateStartString)) {
      alert("입국 일시 '시작' 날짜형식이 올바르지 않습니다");
      return false;
    }
    if (!fnDateCheck(dateEndString)) {
      alert("입국 일시 '종료' 날짜형식이 올바르지 않습니다");
      return false;
    }
  }
  return true;
}

function fnCurrStateSave(obj) {
  localStorage.setItem("obj_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    let objSexTypes = fnGetCode("objSex");
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr class='ques'>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td></td>";
      } else {
        buildHtml = buildHtml + "<td><input type='checkbox' class='chk_info' id='chk_info" + aData.cid + "' value='" + aData.cid + "'><label for='chk_info" + aData.cid + "'><span class='blind'>선택</span></label></td>";
      }
      buildHtml = buildHtml + "<td>" + aData.no + "</td>";
      buildHtml = buildHtml + "<td>" + aData.name + "</td>";
      buildHtml = buildHtml + "<td>" + aData.country + "</td>";
      buildHtml = buildHtml + "<td>" + aData.age + "</td>";
      // let objSexType = "";
      // for (let i = 0; i < objSexTypes.length; i++) {
      //   let aKey = Object.keys(objSexTypes[i])[0];
      //   let aValue = objSexTypes[i][aKey];
      //   if (aKey === aData.sex) objSexType = aValue;
      // }
      buildHtml = buildHtml + "<td>" + aData.sex + "</td>";

      if (aData.entry_date === null) {
        buildHtml = buildHtml + "<td>-</td>";
      } else {
        buildHtml = buildHtml + "<td>" + dateFormat("compactTime", aData.entry_date.date) + "</td>";
      }    

      buildHtml = buildHtml + "<td>" + aData.vehicle_info + "</td>";
      buildHtml = buildHtml + "<td>" + aData.departure_place + "</td>";
      buildHtml = buildHtml + "<td><button type='button' class='accBtn'><span class='accBtn blind'>아코디언 열고닫기</span></button></td>";
      buildHtml = buildHtml + "</tr>";
      buildHtml = buildHtml + "<tr class='answer'>";
      buildHtml = buildHtml + "<td colspan='8'><div class='scr_box'><strong>오염 정보</strong>";      

      buildHtml = buildHtml + "<p>" + aData.contaminant_detail.replace(/\n/gi,"<br>") + "</p>";      
      buildHtml = buildHtml + "</div></td>";      
      buildHtml = buildHtml + "<td colspan='2'><div class='btn_acco'>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<button type='button' class='btn_mod btn_disable' disabled>수정</button>";
      } else {
        buildHtml = buildHtml + "<button type='button' class='btn_mod' value='" + aData.cid + "'>수정</button>";
      }
      buildHtml = buildHtml + "</div></td>";
      buildHtml = buildHtml + "</tr>";

      $("#ownTable > tbody:last").append(buildHtml);
      $(".answer").hide();
    }

    // accodian
    $(".accBtn").click(function(e){
      if ($(this).closest('tr').next('tr').css("display") === "none") {
        $(this).closest("tr").attr("class", "open");
        $(this).closest('tr').next('tr').css("display", "");
      } else {
        $(this).closest("tr").attr("class", "ques");
        $(this).closest('tr').next('tr').css("display", "none");
      }
    });

    // selectbox click event setting
    $("#chk_all").click(function(e){      
      let chkInfos = $(".chk_info:checkbox").toArray();
      for (let i = 0; i < chkInfos.length; i++) {
        let aData = chkInfos[i];
        $(aData).prop("checked", $(this).is(":checked"));
      }
    });

    $(".chk_info").click(function(e){
      let totalCount = $(".chk_info:checkbox").toArray().length;
      let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

      console.log(totalCount + " " + checkedCount);

      let chkAllCheckd = false;
      chkAllCheckd = (totalCount === checkedCount) ? true : false;
      $("#chk_all").prop("checked", chkAllCheckd);
    });


    $(".btn_mod").click(function(e){
      e.preventDefault();
      localStorage.setItem("obj_mod", $(this).val());
      $(location).attr("href", "./object_mod.html");
    });

    $("a.btn_del").unbind("click");
    $("a.btn_del").click(function(e){
      e.preventDefault();
      let checkedData = $(".chk_info:checkbox:checked").toArray();
      if (checkedData.length === 0) {
        alert("삭제할 데이터를 선택해주세요");
        return;
      }

      if (confirm("삭제하시겠습니까?")) {
        let delArr = new Array();
        for (let i = 0; i < checkedData.length; i++) {
          let aData = checkedData[i];
          delArr.push(Number($(aData).val()));
        }
        
        console.log(delArr);
  
        let reqObj = {
          action : "delete",
          delArr : delArr
        };
    
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/object_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            if(result.errCode === 0) {
              let movePage = pager.currPage;
              if (pager.currPage === 1) movePage = 1;
              else {
                if (delArr.length === pager.defPagerPageSize) {
                  movePage = pager.currPage - 1;
                }
              }
              fnGetData(movePage);
            } else {
              alert(result.errMsg);
            }
          }
        });
      }
    });

    // Draw Pager
    fnDrawPager(data, fnGetData);
  } else {
    $("#ownTable > tbody:last").append("<tr><td colspan='10'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}
