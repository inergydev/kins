$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);
  fnVisibleElement("container", true);
  objUserInfo = userInfo;

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);
  if (pagePrevUrl === "response_mod") fnPrevDataSet();
  else if (pagePrevUrl === "response_reg") {
    let isPrevDataSync = localStorage.getItem("res_reg");
    console.log(isPrevDataSync);
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("res_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("res_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)

    $("#listInspArea").prop("checked", false);
    $("#listContactName").prop("checked", false);
    $("#listDepartment").prop("checked", false);
    $("#listMonitoringPlace").prop("checked", false);
    $("#listTesterSettingLoc").prop("checked", false);
    $("#listOperEquipName").prop("checked", false);


    for (let i = 0; i < obj.searchOptions.length; i++) {
      let aData = obj.searchOptions[i];
      if (aData === "insp_area") $("#listInspArea").prop("checked", true);
      if (aData === "contact_name") $("#listContactName").prop("checked", true);
      if (aData === "department") $("#listDepartment").prop("checked", true);
      if (aData === "monitoring_place") $("#listMonitoringPlace").prop("checked", true);
      if (aData === "tester_setting_loc") $("#listTesterSettingLoc").prop("checked", true);
      if (aData === "oper_equip_name") $("#listOperEquipName").prop("checked", true);
    }
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();
  let option_1 = $("#listInspArea").is(":checked");
  let option_2 = $("#listContactName").is(":checked");
  let option_3 = $("#listDepartment").is(":checked");
  let option_4 = $("#listMonitoringPlace").is(":checked");
  let option_5 = $("#listTesterSettingLoc").is(":checked");
  let option_6 = $("#listOperEquipName").is(":checked");

  if (!option_1 && !option_2 && !option_3 && !option_4 && !option_5 && !option_6) {
    alert("검색옵션을 1개 이상 선택해주세요");
    return false;
  }

  if ((option_1 || option_2 || option_3 || option_4 || option_5) && searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  let searchOptions = new Array();
  if ($("#listInspArea").is(":checked")) searchOptions.push("insp_area");
  if ($("#listContactName").is(":checked")) searchOptions.push("contact_name");
  if ($("#listDepartment").is(":checked")) searchOptions.push("department");
  if ($("#listMonitoringPlace").is(":checked")) searchOptions.push("monitoring_place");
  if ($("#listTesterSettingLoc").is(":checked")) searchOptions.push("tester_setting_loc");
  if ($("#listOperEquipName").is(":checked")) searchOptions.push("oper_equip_name");

  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val(),
    searchOptions : searchOptions
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/response_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
        fnVisibleElement("container", true);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnCurrStateSave(obj) {
  localStorage.setItem("res_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    
    let resAreaTypes = fnGetCode("resArea");
    let resAirSeaTypes = fnGetCode("resAirSea");
    let resEquipNameTypes = fnGetCode("resEquipName");
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td rowspan='3'></td>";
      } else {
        buildHtml = buildHtml + "<td rowspan='3'><input type='checkbox' class='chk_info' id='chk_info" + aData.cid + "' value='" + aData.cid + "'><label for='chk_info" + aData.cid + "'><span class='blind'>선택</span></label></td>";
      }
      buildHtml = buildHtml + "<td rowspan='3'>" + aData.no + "</td>";
      buildHtml = buildHtml + "<th scope='row'>지역</th>";

      // let resAreaType = "";
      // for (let i = 0; i < resAreaTypes.length; i++) {
      //   let aKey = Object.keys(resAreaTypes[i])[0];
      //   let aValue = resAreaTypes[i][aKey];
      //   if (aKey === aData.insp_area) resAreaType = aValue;
      // }
      buildHtml = buildHtml + "<td>" + aData.insp_area + "</td>";
      buildHtml = buildHtml + "<th scope='row'>현장담당자 이름</th>";
      buildHtml = buildHtml + "<td>" + aData.contact_name + "</td>";
      buildHtml = buildHtml + "<th scope='row'>오염검사기 위치</th>";
      buildHtml = buildHtml + "<td>" + aData.tester_setting_loc + "</td>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td rowspan='3'><button type='button' class='btn_mod btn_disable' disabled>수정</button></td>";
      } else {
        buildHtml = buildHtml + "<td rowspan='3'><button type='button' class='btn_mod' value='" + aData.cid + "'>수정</button></td>";
      }
      buildHtml = buildHtml + "</tr>";

      buildHtml = buildHtml + "<tr>";
      buildHtml = buildHtml + "<th scope='row'>공항/항만</th>";

      // let resAirSeaType = "";
      // for (let i = 0; i < resAirSeaTypes.length; i++) {
      //   let aKey = Object.keys(resAirSeaTypes[i])[0];
      //   let aValue = resAirSeaTypes[i][aKey];
      //   if (aKey === aData.insp_air_sea) resAirSeaType = aValue;
      // }
      buildHtml = buildHtml + "<td>" + aData.insp_air_sea + "</td>";
      buildHtml = buildHtml + "<th scope='row'>소속기관</th>";
      buildHtml = buildHtml + "<td>" + aData.department + "</td>";

      // let resEquipNameType = "";
      // for (let i = 0; i < resEquipNameTypes.length; i++) {
      //   let aKey = Object.keys(resEquipNameTypes[i])[0];
      //   let aValue = resEquipNameTypes[i][aKey];
      //   if (aKey === aData.oper_equip_name) resEquipNameType = aValue;
      // }     
      buildHtml = buildHtml + "<th scope='row'>현장운영장비</th>";
      buildHtml = buildHtml + "<td>" + aData.oper_equip_name + "</td>";
      buildHtml = buildHtml + "</tr>";

      buildHtml = buildHtml + "<tr>";
      buildHtml = buildHtml + "<th scope='row'>계측장소</th>";
      buildHtml = buildHtml + "<td>" + aData.insp_place + "</td>";
      buildHtml = buildHtml + "<th scope='row'>모니터링 장소</th>";
      buildHtml = buildHtml + "<td>" + aData.monitoring_place + "</td>";
      buildHtml = buildHtml + "<th scope='row'>수량</th>";
      buildHtml = buildHtml + "<td>" + aData.oper_equip_count + "</td>";
      buildHtml = buildHtml + "</tr>";

      $("#ownTable > tbody:last").append(buildHtml);
    }

    // selectbox click event setting
    $("#chk_all").click(function(e){      
      let chkInfos = $(".chk_info:checkbox").toArray();
      for (let i = 0; i < chkInfos.length; i++) {
        let aData = chkInfos[i];
        $(aData).prop("checked", $(this).is(":checked"));
      }
    });

    $(".chk_info").click(function(e){
      let totalCount = $(".chk_info:checkbox").toArray().length;
      let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

      console.log(totalCount + " " + checkedCount);

      let chkAllCheckd = false;
      chkAllCheckd = (totalCount === checkedCount) ? true : false;
      $("#chk_all").prop("checked", chkAllCheckd);
    });


    $(".btn_mod").click(function(e){
      e.preventDefault();
      localStorage.setItem("res_mod", $(this).val());
      $(location).attr("href", "./response_mod.html");
    });

    $("a.btn_del").unbind("click");
    $("a.btn_del").click(function(e){
      e.preventDefault();
      let checkedData = $(".chk_info:checkbox:checked").toArray();
      if (checkedData.length === 0) {
        alert("삭제할 데이터를 선택해주세요");
        return;
      }

      if (confirm("삭제하시겠습니까?")) {
        let delArr = new Array();
        for (let i = 0; i < checkedData.length; i++) {
          let aData = checkedData[i];
          delArr.push(Number($(aData).val()));
        }
        
        console.log(delArr);
  
        let reqObj = {
          action : "delete",
          delArr : delArr
        };
    
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/response_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            if(result.errCode === 0) {
              let movePage = pager.currPage;
              if (pager.currPage === 1) movePage = 1;
              else {
                if (delArr.length === pager.defPagerPageSize) {
                  movePage = pager.currPage - 1;
                }
              }
              fnGetData(movePage);
            } else {
              alert(result.errMsg);
            }
          }
        });
      }
    });

    // Draw Pager
    fnDrawPager(data, fnGetData);
  } else {
    $("#ownTable > tbody:last").append("<tr><td colspan='10'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}
