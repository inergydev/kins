$(document).ready(function(){

  fnCheckSession(true, "./main.html");

  $(".input_area").keyup(function(e) {
    if (e.keyCode == 13) $(".login_btn").click();
  });

  $(".login_btn").click(function() {
    let id = $("#id").val();
    let pwd = $("#password").val();
    let descSpan = $("#login_desc_txt");

    // Validation Check
    if (id.length <= 0) {
      descSpan.html("아이디를 입력해주세요.");
      descSpan.attr('class', 'error');
      return;
    }
    else if (pwd.length <=0) {
      descSpan.html("패스워드를 입력해주세요.");
      descSpan.attr('class', 'error');
      return;
    }
    
    // Request Object Setting
    let reqObj = {id:id, pwd:pwd};

    // API Call
    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/login_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./main.html");
        } else {
          descSpan.html("아이디 또는 비밀번호를 다시 확인하세요.<br>등록되지 않은 아이디이거나, 아이디 또는 비밀번호를 잘못 입력하셨습니다");
          descSpan.attr('class', 'error');
        }
      }
    });
  });
});