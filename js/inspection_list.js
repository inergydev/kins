$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);
  fnVisibleElement("container", true);
  objUserInfo = userInfo;

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);

  console.log(pagePrevUrl);

  if (pagePrevUrl === "inspection_mod") fnPrevDataSet();
  else if (pagePrevUrl === "inspection_reg") {
    let isPrevDataSync = localStorage.getItem("ins_reg");
    console.log(isPrevDataSync);
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("ins_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if(!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if(!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });

  $("#listInspDate").click(function(e){
    let checked = $("#listInspDate").is(":checked");
    $("#listInspStartDate").prop('disabled', !checked);
    $("#listInspEndDate").prop('disabled', !checked);
  });

  $("#listInspStartDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#listInspEndDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("ins_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)
    $("#listInspDate").prop("checked", false);
    $("#listInspAirSea").prop("checked", false);
    $("#listRegisterPoint").prop("checked", false);


    if (obj.searchDateOption) {
      $("#listInspDate").prop("checked", true);
      $("#listInspStartDate").prop("disabled", false);
      $("#listInspEndDate").prop("disabled", false);
      $("#listInspStartDate").val(obj.dateStart.replace(/-/gi, ""));
      $("#listInspEndDate").val(obj.dateEnd.replace(/-/gi, ""));
    }

    for (let i = 0; i < obj.searchOptions.length; i++) {
      let aData = obj.searchOptions[i];
      if (aData === "insp_air_sea") $("#listInspAirSea").prop("checked", true);
      if (aData === "insp_air_sea_desc") $("#listInspAirSea").prop("checked", true);
      if (aData === "register_point") $("#listRegisterPoint").prop("checked", true);
    }
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();
  let option_1 = $("#listInspDate").is(":checked");
  let option_2 = $("#listInspAirSea").is(":checked");
  let option_3 = $("#listRegisterPoint").is(":checked");

  if (!option_1 && !option_2 && !option_3) {
    alert("검색옵션을 1개 이상 선택해주세요");
    return false;
  }

  if ((option_2 || option_3) && searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  if(!fnValidationCheck()) return;

  let searchOptions = new Array();
  if ($("#listInspAirSea").is(":checked")) searchOptions.push("insp_air_sea");
  if ($("#listInspAirSea").is(":checked")) searchOptions.push("insp_air_sea_desc");
  if ($("#listRegisterPoint").is(":checked")) searchOptions.push("register_point");

  let dateStartString = $("#listInspStartDate").val();
  let dateEndString = $("#listInspEndDate").val();
  let isSearchDateOption = false;
  if ($("#listInspDate").is(":checked")) {
    isSearchDateOption = true;
    dateStartString = dateStartString.substring(0, 4) + "-" + dateStartString.substring(4, 6) + "-" + dateStartString.substring(6, 8);
    dateEndString = dateEndString.substring(0, 4) + "-" + dateEndString.substring(4, 6) + "-" + dateEndString.substring(6, 8);
  }

  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val(),
    searchOptions : searchOptions,
    searchDateOption : isSearchDateOption,
    dateStart : dateStartString,
    dateEnd : dateEndString,
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/inspection_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
        fnVisibleElement("container", true);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnValidationCheck() {
  if ($("#listInspDate").is(":checked")) {
    let dateStartString = $("#listInspStartDate").val();
    let dateEndString = $("#listInspEndDate").val();

    if (!fnDateCheck(dateStartString)) {
      alert("사고 일시 '시작' 날짜형식이 올바르지 않습니다");
      return false;
    }
    if (!fnDateCheck(dateEndString)) {
      alert("사고 일시 '종료' 날짜형식이 올바르지 않습니다");
      return false;
    }
  }
  return true;
}

function fnCurrStateSave(obj) {
  localStorage.setItem("ins_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    
    let insAirSeaTypes = fnGetCode("insAirSea");
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td></td>";
      } else {
        buildHtml = buildHtml + "<td><input type='checkbox' class='chk_info' id='chk_info" + aData.cid + "' value='" + aData.cid + "'><label for='chk_info" + aData.cid + "'><span class='blind'>선택</span></label></td>";
      }
      buildHtml = buildHtml + "<td>" + aData.no + "</td>";
      if (aData.insp_date_time === null) {
        buildHtml = buildHtml + "<td>-</td>";
      } else {
        buildHtml = buildHtml + "<td>" + dateFormat("compactTime", aData.insp_date_time.date) + "</td>";
      }    
      // let insAirType = "";
      // for (let i = 0; i < insAirSeaTypes.length; i++) {
      //   let aKey = Object.keys(insAirSeaTypes[i])[0];
      //   let aValue = insAirSeaTypes[i][aKey];
      //   if (aKey === aData.insp_air_sea) insAirType = aValue;
      // }

      buildHtml = buildHtml + "<td>[" + aData.insp_air_sea + "]</td>";
      buildHtml = buildHtml + "<td>" + aData.insp_air_sea_desc + "</td>";
      buildHtml = buildHtml + "<td>" + aData.register_point + "</td>";
      buildHtml = buildHtml + "<td>" + aData.arrival_num + "</td>";
      buildHtml = buildHtml + "<td>" + aData.passenger_num + "</td>";
      buildHtml = buildHtml + "<td>" + aData.insp_tot_num + "</td>";

      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td><button type='button' class='btn_mod btn_disable' disabled>수정</button></td>";
      } else {
        buildHtml = buildHtml + "<td><button type='button' class='btn_mod' value='" + aData.cid + "'>수정</button></td>";
      }
      buildHtml = buildHtml + "</tr>";

      $("#ownTable > tbody:last").append(buildHtml);
    }

    // selectbox click event setting
    $("#chk_all").click(function(e){      
      let chkInfos = $(".chk_info:checkbox").toArray();
      for (let i = 0; i < chkInfos.length; i++) {
        let aData = chkInfos[i];
        $(aData).prop("checked", $(this).is(":checked"));
      }
    });

    $(".chk_info").click(function(e){
      let totalCount = $(".chk_info:checkbox").toArray().length;
      let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

      console.log(totalCount + " " + checkedCount);

      let chkAllCheckd = false;
      chkAllCheckd = (totalCount === checkedCount) ? true : false;
      $("#chk_all").prop("checked", chkAllCheckd);
    });


    $(".btn_mod").click(function(e){
      e.preventDefault();
      localStorage.setItem("ins_mod", $(this).val());
      $(location).attr("href", "./inspection_mod.html");
    });

    $("a.btn_del").unbind("click");
    $("a.btn_del").click(function(e){
      e.preventDefault();
      let checkedData = $(".chk_info:checkbox:checked").toArray();
      if (checkedData.length === 0) {
        alert("삭제할 데이터를 선택해주세요");
        return;
      }

      if (confirm("삭제하시겠습니까?")) {
        let delArr = new Array();
        for (let i = 0; i < checkedData.length; i++) {
          let aData = checkedData[i];
          delArr.push(Number($(aData).val()));
        }
        
        console.log(delArr);
  
        let reqObj = {
          action : "delete",
          delArr : delArr
        };
    
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/inspection_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            if(result.errCode === 0) {
              let movePage = pager.currPage;
              if (pager.currPage === 1) movePage = 1;
              else {
                if (delArr.length === pager.defPagerPageSize) {
                  movePage = pager.currPage - 1;
                }
              }
              fnGetData(movePage);
            } else {
              alert(result.errMsg);
            }
          }
        });
      }
    });

    // Draw Pager
    fnDrawPager(data, fnGetData);
  } else {
    $("#ownTable > tbody:last").append("<tr><td colspan='10'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}
