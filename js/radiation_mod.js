$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  let cid = localStorage.getItem("rad_mod");

  if (cid === null || cid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./radiation_list.html");
      return;
    }
  }

  $("#modAccidentYear").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAccidentMonth").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAccidentDay").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  fnGetData(cid);

  $("#modBtn").click(function(e){
    fnModData(cid);
  });
}

function fnGetData(cid) {
  let reqObj = {
    action : "aSelect",
    cid : cid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/radiation_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            $(location).attr("href", "./radiation_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnModData(cid) {
  if(!fnValidationCheck()) return;

  let dateString = $("#modAccidentYear").val() + "-" + pad($("#modAccidentMonth").val(), 2) + "-" +  pad($("#modAccidentDay").val(), 2);
  
  if(confirm("수정하시겠습니까?")) {
    let reqObj = {
      action : "update",
      cid : cid,
      country : $("#modCountry").val(),
      loc : $("#modAccidentLoc").val(),
      facility : $("#modAccidentFacility").val(),
      date : dateString,
      level : $("#modAccidentLevel").val(),
      kinsLevel : $("#modAccidentKinsLevel").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/radiation_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./radiation_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnDrawView(data) {
  $("#modCountry").val(data.info.country);
  $("#modAccidentLoc").val(data.info.accident_loc);
  $("#modAccidentFacility").val(data.info.accident_facility);
  $("#modAccidentLevel").val(data.info.accident_level);
  $("#modAccidentKinsLevel").val(data.info.accident_kins_level);

  if (data.info.accident_date !== null) {
    let dateString = dateFormat("full", data.info.accident_date.date);
    dateString = dateString.substring(0, 10);
    let dateArr = dateString.split("-");
    $("#modAccidentYear").val(dateArr[0]);
    $("#modAccidentMonth").val(dateArr[1]);
    $("#modAccidentDay").val(dateArr[2]);

  }
}

function fnValidationCheck() {
  if ($("#modCountry").val().length >= 30) {
    alert("국가는 30자 이내여야 합니다");
    return false;
  } else if ($("#modCountry").val().length === 0) {
    alert("국가를 입력해주세요");
    return false;
  }
  if ($("#modAccidentLoc").val().length >= 30) {
    alert("사고 위치는 30자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentLoc").val().length === 0) {
    alert("사고 위치를 입력해주세요");
    return false;
  }
  if ($("#modAccidentFacility").val().length >= 30) {
    alert("사고 시설은 30자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentFacility").val().length === 0) {
    alert("사고 시설을 입력해주세요");
    return false;
  }
  let dateString = $("#modAccidentYear").val() + pad($("#modAccidentMonth").val(), 2) + pad($("#modAccidentDay").val(), 2);
  if (!fnDateCheck(dateString)) {
    alert("사고 일시의 날짜형식이 올바르지 않습니다.");
    return false;
  }
  if ($("#modAccidentLevel").val().length >= 30) {
    alert("사고 등급은 5자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentLevel").val().length === 0) {
    alert("사고 등급을 입력해주세요");
    return false;
  }
  if ($("#modAccidentKinsLevel").val().length >= 30) {
    alert("원자력 안전위원회 위기경보등급은 10자 이내여야 합니다");
    return false;
  } else if ($("#modAccidentKinsLevel").val().length === 0) {
    alert("원자력 안전위원회 위기경보등급을 입력해주세요");
    return false;
  }
  return true;
}