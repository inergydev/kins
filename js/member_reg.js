$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  if (userInfo.USER_AUTH === "ADMIN") {
    $("#modSelUserAuth").children("[value='ADMIN']").remove();
  }

  $("#regBtn").click(function(e){
    fnRegData();
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("member_reg", false);
    $(location).attr("href", "./member_list.html");
  });
}

function fnRegData() {
  if(!fnValidationCheck()) return;
  
  if(confirm("등록하시겠습니까?")) {
    let reqObj = {
      action : "insert",
      orgName : $("#modOrgName").val(),
      contactName : $("#modContactName").val(),
      rank : $("#modRank").val(),
      id : $("#modId").val(),
      pwd : $("#modPwd").val(),
      useYn : $("#modSelUseYn").val(),
      userAuth : $("#modSelUserAuth").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/member_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          localStorage.setItem("member_reg", true);
          $(location).attr("href", "./member_list.html");
        } else if (result.errCode === 2){
          alert("이미 존재하는 ID입니다");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modOrgName").val().length >= 20) {
    alert("기관명은 20자 이내여야 합니다");
    return false;
  } else if ($("#modOrgName").val().length === 0) {
    alert("기관명을 입력해주세요");
    return false;
  }

  if ($("#modContactName").val().length >= 20) {
    alert("담당자는 20자 이내여야 합니다");
    return false;
  } else if ($("#modContactName").val().length === 0) {
    alert("담당자를 입력해주세요");
    return false;
  }

  if ($("#modRank").val().length >= 10) {
    alert("직급은 10자 이내여야 합니다");
    return false;
  } else if ($("#modRank").val().length === 0) {
    alert("직급을 입력해주세요");
    return false;
  }

  if ($("#modId").val().length >= 20) {
    alert("ID는 20자 이내여야 합니다");
    return false;
  } else if ($("#modId").val().length === 0) {
    alert("ID를 입력해주세요");
    return false;
  }

  if ($("#modPwd").val().length >= 20) {
    alert("PW는 20자 이내여야 합니다");
    return false;
  } else if ($("#modPwd").val().length === 0) {
    alert("PW를 입력해주세요");
    return false;
  }

  return true;
}
