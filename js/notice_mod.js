$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

isAttachFile = false;
isAttachFileDelete = false;
attachState = "none"; // insert, update, delete, none

function fnCheckSessionCallback(userInfo) {
  let cid = localStorage.getItem("notice_view");

  if (cid === null || cid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./notice_list.html");
      return;
    }
  }

  $("#attachFile").attr("accept", gFileUploadAllowExtention);

  $("#attachRegBtn").click(function(e){
    $("#attachFile").click();
  });

  $("#attachFile").change(function(e){

    let file = $("#attachFile").prop("files")[0];
    let fileName = file.name;

    $("#attachPreview").html("");
    $("#attachPreview").html(fileName + " <button class='del_file' id='attachDelBtn'><span class='blind'>파일삭제</span></button>");

    $("#attachDelBtn").unbind("click");
    $("#attachDelBtn").click(function(e){
      $("#attachPreview").html("");
      $('#attachFile').val("");
    });
  });

  fnGetData(cid);

  $("#modBtn").click(function(e){
    fnModData(cid);
  });
}

function fnGetData(cid) {
  let reqObj = {
    action : "aSelect",
    cid : cid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/notice_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            $(location).attr("href", "./notice_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnModData(cid) {
  if(!fnValidationCheck()) return;
  
  if(confirm("수정하시겠습니까?")) {
    let formData = new FormData();
    formData.append("action", "update");
    formData.append("cid", cid);
    formData.append("title", $("#modTitle").val());
    formData.append("content", $("#modContent").val());

    let file = $("#attachFile").prop("files")[0];
    if (file !== undefined) {
      if (isAttachFile) attachState = "update";
      else attachState = "insert";
      formData.append("file", $("#attachFile")[0].files[0]);
    } else {
      if (isAttachFile && isAttachFileDelete) attachState = "delete";
      else if (!isAttachFile && !isAttachFileDelete) attachState = "none";
    }

    formData.append("attachState", attachState);

    console.log(attachState);

    $.ajax({
      type: "POST",
      contentType: "multipart/form-data",
      url: "../php/notice_api.php",
      cache: false,
      processData: false,
      dataType: 'json',
      mimeType: 'multipart/form-data',
      contentType: false,
      processData: false,
      data: formData,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./notice_view.html");
        } else if (result.errCode === 2) {
          alert("파일업로드에 실패하였습니다");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnDrawView(data) {
  $("#modTitle").val(data.info.title);
  $("#modContent").val(data.info.contents);

  if (data.info.attach_r_name !== null && data.info.attach_r_name.length > 0) {
    isAttachFile = true;

    $("#attachPreview").html("");
    $("#attachPreview").html(data.info.attach_r_name + " <button class='del_file' id='attachDelBtn'><span class='blind'>파일삭제</span></button>");

    $("#attachDelBtn").click(function(e){
      $("#attachPreview").html("");
      $('#attachFile').val("");
      isAttachFileDelete = true;
    });
  }
}

function fnValidationCheck() {
  if ($("#modTitle").val().length >= 100) {
    alert("제목은 100자 이내여야 합니다");
    return false;
  } else if ($("#modTitle").val().length === 0) {
    alert("제목을 입력해주세요");
    return false;
  }

  if ($("#modContent").val().length === 0) {
    alert("내용을 입력해주세요");
    return false;
  }
  
  return true;
}
