$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  let uid = localStorage.getItem("member_mod");

  if (uid === null || uid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./member_list.html");
      return;
    }
  }

  if (userInfo.USER_AUTH === "ADMIN") {
    $("#modSelUserAuth").children("[value='ADMIN']").remove();
  }

  fnGetData(uid);

  $("#modBtn").click(function(e){
    fnModData(uid);
  });
}

function fnGetData(uid) {
  let reqObj = {
    action : "aSelect",
    uid : uid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/member_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            $(location).attr("href", "./member_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnModData(uid) {
  if(!fnValidationCheck()) return;
  
  if(confirm("수정하시겠습니까?")) {
    let reqObj = {
      action : "update",
      uid : uid,
      orgName : $("#modOrgName").val(),
      contactName : $("#modContactName").val(),
      rank : $("#modRank").val(),
      id : $("#modId").val(),
      pwd : $("#modPwd").val(),
      useYn : $("#modSelUseYn").val(),
      userAuth : $("#modSelUserAuth").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/member_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./member_list.html");
        } else if (result.errCode === 2){
          alert("이미 존재하는 ID입니다");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnDrawView(data) {
  $("#modOrgName").val(data.info.org_name);
  $("#modContactName").val(data.info.contact_name);
  $("#modRank").val(data.info.rank);
  $("#modId").val(data.info.id);
  $("#modPwd").val(data.info.pwd);
  $("#registDate").text(dateFormat("full", data.info.regist_date.date));
  if (data.info.last_access_date !== null) {
    $("#lastAccessDate").text(dateFormat("full", data.info.last_access_date.date));
  } else {
    $("#lastAccessDate").text("-");
  }
  $("#modSelUseYn").val(data.info.use_yn);
  $("#modSelUserAuth").val(data.info.user_auth);
}

function fnValidationCheck() {
  if ($("#modOrgName").val().length >= 20) {
    alert("기관명은 20자 이내여야 합니다");
    return false;
  } else if ($("#modOrgName").val().length === 0) {
    alert("기관명을 입력해주세요");
    return false;
  }

  if ($("#modContactName").val().length >= 20) {
    alert("담당자는 20자 이내여야 합니다");
    return false;
  } else if ($("#modContactName").val().length === 0) {
    alert("담당자를 입력해주세요");
    return false;
  }

  if ($("#modRank").val().length >= 10) {
    alert("직급은 10자 이내여야 합니다");
    return false;
  } else if ($("#modRank").val().length === 0) {
    alert("직급을 입력해주세요");
    return false;
  }

  if ($("#modId").val().length >= 20) {
    alert("ID는 20자 이내여야 합니다");
    return false;
  } else if ($("#modId").val().length === 0) {
    alert("ID를 입력해주세요");
    return false;
  }

  if ($("#modPwd").val().length >= 20) {
    alert("PW는 20자 이내여야 합니다");
    return false;
  } else if ($("#modPwd").val().length === 0) {
    alert("PW를 입력해주세요");
    return false;
  }

  return true;
}
