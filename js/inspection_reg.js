$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  $("#regBtn").click(function(e){
    fnRegData(userInfo);
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("rad_reg", false);
    $(location).attr("href", "./inspection_list.html");
  });

  $("#modInspYear").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modInspMonth").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modInspDay").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
  
  $("#modInspHour").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modInspMinute").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modArrivalNum").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modPassengerNum").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modTotNum").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modInspAirSeaType").append("<option value='' selected disabled>선택</option>");
  let types = fnGetCode("insAirSea");
  for (let i = 0; i < types.length; i++) {
    let aData = types[i];
    console.log(aData);
    let html = "<option value='" + aData + "'>" + aData + "</option>";
    $("#modInspAirSeaType").append(html);
  }
}

function fnRegData(userInfo) {
  if(!fnValidationCheck()) return;
  let dateString = $("#modInspYear").val() + "-" + pad($("#modInspMonth").val(), 2) + "-" +  pad($("#modInspDay").val(), 2) +  "-" + pad($("#modInspHour").val(), 2) +  "-" + pad($("#modInspMinute").val(), 2);

  console.log(dateString);

  if(confirm("등록하시겠습니까?")) {
    let reqObj = {
      action : "insert",
      uid : userInfo.UID,
      date : dateString,
      airSeaType : $("#modInspAirSeaType").val(),
      airSeaDesc : $("#modInspAirSeaDesc").val(),
      registerPoint : $("#modRegisterPoint").val(),
      arrivalNum : Number($("#modArrivalNum").val()),
      passengerNum : Number($("#modPassengerNum").val()),
      totNum : Number($("#modTotNum").val())
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/inspection_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          localStorage.setItem("ins_reg", true);
          $(location).attr("href", "./inspection_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  let dateString = $("#modInspYear").val() + pad($("#modInspMonth").val(), 2) + pad($("#modInspDay").val(), 2);
  if (!fnDateCheck(dateString)) {
    alert("날짜의 형식이 올바르지 않습니다.");
    return false;
  }

  let hour = Number($("#modInspHour").val());
  let minute = Number($("#modInspMinute").val());
  
  if ($("#modInspHour").val().length === 0) {
    alert("시간을 입력해주세요.");
    return false;
  }

  if (hour > 24) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modInspMinute").val().length === 0) {
    alert("분을 입력해주세요.");
    return false;
  }

  if (minute > 59) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modInspAirSeaType").val() === null) {
    alert("여객정보를 선택해주세요.");
    return false;
  }

  if ($("#modInspAirSeaDesc").val().length >= 20) {
    alert("여객정보는 20자 이내여야 합니다");
    return false;
  } else if ($("#modInspAirSeaDesc").val().length === 0) {
    alert("여객정보를 입력해주세요");
    return false;
  }

  if ($("#modRegisterPoint").val().length >= 20) {
    alert("등록지점은 20자 이내여야 합니다");
    return false;
  } else if ($("#modRegisterPoint").val().length === 0) {
    alert("등록지점을 입력해주세요");
    return false;
  }

  if ($("#modArrivalNum").val().length === 0) {
    alert("입국자수를 입력해주세요");
    return false;
  }

  if ($("#modPassengerNum").val().length === 0) {
    alert("탑승자수를 입력해주세요");
    return false;
  }

  if ($("#modTotNum").val().length === 0) {
    alert("전체 검사 인원 수를 입력해주세요");
    return false;
  }

  return true;
}