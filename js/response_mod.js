$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  let cid = localStorage.getItem("res_mod");

  if (cid === null || cid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./response_list.html");
      return;
    }
  }

  $("#modBtn").click(function(e){
    fnModData(cid);
  });

  $("#modInspArea").append("<option value='' selected disabled>선택</option>");
  let types = fnGetCode("resArea");
  for (let i = 0; i < types.length; i++) {
    let aKey = Object.keys(types[i])[0];
    let html = "<option value='" + aKey + "'>" + aKey + "</option>";
    $("#modInspArea").append(html);
  }

  $("#modInspArea").change(function() { 
    console.log($(this).val());
    let value = $(this).val();
    let types = fnGetCode("resArea");

    for (let i = 0; i < types.length; i++) {
      let aKey = Object.keys(types[i])[0];
      if (aKey === value) {
        let datas = types[i][aKey];
        console.log(datas);
        $("#modInspAirSea").html("");
        $("#modInspAirSea").append("<option value='' selected disabled>선택</option>");
        for (let j = 0; j < datas.length; j++) {
          let aKey = datas[j];
          let html = "<option value='" + aKey + "'>" + aKey + "</option>";
          $("#modInspAirSea").append(html);
        }
      }
    }
  });
  
  $("#modOperEquipName").append("<option value='' selected disabled>선택</option>");
  types = fnGetCode("resEquipName");
  for (let i = 0; i < types.length; i++) {
    let aData = types[i];
    let html = "<option value='" + aData + "'>" + aData + "</option>";
    $("#modOperEquipName").append(html);
  }


  $("#modDperEquipCount").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  fnGetData(cid);
}

function fnGetData(cid) {
  let reqObj = {
    action : "aSelect",
    cid : cid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/response_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            $(location).attr("href", "./response_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnDrawView(data) {
  $("#modInspArea").val(data.info.insp_area);

  let types = fnGetCode("resArea");
  for (let i = 0; i < types.length; i++) {
    let aKey = Object.keys(types[i])[0];
    if (aKey === data.info.insp_area) {
      let datas = types[i][aKey];
      console.log(datas);
      $("#modInspAirSea").html("");
      $("#modInspAirSea").append("<option value='' selected disabled>선택</option>");
      for (let j = 0; j < datas.length; j++) {
        let aKey = datas[j];
        let html = "<option value='" + aKey + "'>" + aKey + "</option>";
        $("#modInspAirSea").append(html);
      }
    }
  }

  $("#modInspAirSea").val(data.info.insp_air_sea);
  $("#modInsPlace").val(data.info.insp_place);
  $("#modContactName").val(data.info.contact_name);
  $("#modDepartment").val(data.info.department);
  $("#modMonitoringPlace").val(data.info.monitoring_place);
  $("#modTesterSettingLoc").val(data.info.tester_setting_loc);
  $("#modOperEquipName").val(data.info.oper_equip_name);
  $("#modDperEquipCount").val(data.info.oper_equip_count);
}

function fnModData(cid) {
  if(!fnValidationCheck()) return;

  if(confirm("수정하시겠습니까?")) {
    let reqObj = {
      action : "update",
      cid : cid,
      inspArea : $("#modInspArea").val(),
      inspAirSea : $("#modInspAirSea").val(),
      insPlace : $("#modInsPlace").val(),
      contactName : $("#modContactName").val(),
      department : $("#modDepartment").val(),
      monitoringPlace : $("#modMonitoringPlace").val(),
      testerSettingLoc : $("#modTesterSettingLoc").val(),
      operEquipName : $("#modOperEquipName").val(),
      operEquipCount : $("#modDperEquipCount").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/response_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./response_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modInspArea").val().length === 0) {
    alert("지역을 선택해주세요");
    return false;
  } 

  if ($("#modInspAirSea").val().length === 0) {
    alert("공항/항만을 선택해주세요");
    return false;
  } 

  if ($("#modInsPlace").val().length >= 20) {
    alert("계측장소는 20자 이내여야 합니다");
    return false;
  } else if ($("#modInsPlace").val().length === 0) {
    alert("계측장소를 입력해주세요");
    return false;
  }

  if ($("#modContactName").val().length >= 20) {
    alert("이름은 20자 이내여야 합니다");
    return false;
  } else if ($("#modContactName").val().length === 0) {
    alert("이름을 입력해주세요");
    return false;
  }

  if ($("#modDepartment").val().length >= 20) {
    alert("근무부서는 20자 이내여야 합니다");
    return false;
  } else if ($("#modDepartment").val().length === 0) {
    alert("근무부서를 입력해주세요");
    return false;
  }

  if ($("#modMonitoringPlace").val().length >= 20) {
    alert("모니터링장소는 20자 이내여야 합니다");
    return false;
  } else if ($("#modMonitoringPlace").val().length === 0) {
    alert("모니터링장소를 입력해주세요");
    return false;
  }

  if ($("#modTesterSettingLoc").val().length >= 20) {
    alert("오염검사기는 20자 이내여야 합니다");
    return false;
  } else if ($("#modTesterSettingLoc").val().length === 0) {
    alert("오염검사기를 입력해주세요");
    return false;
  }

  if ($("#modOperEquipName").val().length === 0) {
    alert("장비를 선택해주세요");
    return false;
  } 

  if ($("#modDperEquipCount").val().length === 0) {
    alert("수량을 입력해주세요");
    return false;
  } 

  return true;
}