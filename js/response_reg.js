$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  $("#regBtn").click(function(e){
    fnRegData(userInfo);
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("res_reg", false);
    $(location).attr("href", "./response_list.html");
  });

  $("#modInspArea").append("<option value='' selected disabled>선택</option>");
  let types = fnGetCode("resArea");
  for (let i = 0; i < types.length; i++) {
    let aKey = Object.keys(types[i])[0];
    let html = "<option value='" + aKey + "'>" + aKey + "</option>";
    $("#modInspArea").append(html);
  }

  $("#modInspArea").change(function() { 
    console.log($(this).val());
    let value = $(this).val();
    let types = fnGetCode("resArea");

    for (let i = 0; i < types.length; i++) {
      let aKey = Object.keys(types[i])[0];
      if (aKey === value) {
        let datas = types[i][aKey];
        console.log(datas);
        $("#modInspAirSea").html("");
        $("#modInspAirSea").append("<option value='' selected disabled>선택</option>");
        for (let j = 0; j < datas.length; j++) {
          let aKey = datas[j];
          let html = "<option value='" + aKey + "'>" + aKey + "</option>";
          $("#modInspAirSea").append(html);
        }
      }
    }
  });
  

  $("#modInspAirSea").append("<option value='' selected disabled>선택</option>");

  $("#modOperEquipName").append("<option value='' selected disabled>선택</option>");
  types = fnGetCode("resEquipName");
  for (let i = 0; i < types.length; i++) {
    let aData = types[i];
    let html = "<option value='" + aData + "'>" + aData + "</option>";
    $("#modOperEquipName").append(html);
  }

  $("#modDperEquipCount").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
}

function fnRegData(userInfo) {
  if(!fnValidationCheck()) return;

  if(confirm("등록하시겠습니까?")) {
    let reqObj = {
      action : "insert",
      uid : userInfo.UID,
      inspArea : $("#modInspArea").val(),
      inspAirSea : $("#modInspAirSea").val(),
      insPlace : $("#modInsPlace").val(),
      contactName : $("#modContactName").val(),
      department : $("#modDepartment").val(),
      monitoringPlace : $("#modMonitoringPlace").val(),
      testerSettingLoc : $("#modTesterSettingLoc").val(),
      operEquipName : $("#modOperEquipName").val(),
      operEquipCount : $("#modDperEquipCount").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/response_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          localStorage.setItem("res_reg", true);
          $(location).attr("href", "./response_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modInspArea").val().length === 0) {
    alert("지역을 선택해주세요");
    return false;
  } 

  if ($("#modInspAirSea").val().length === 0) {
    alert("공항/항만을 선택해주세요");
    return false;
  } 

  if ($("#modInsPlace").val().length >= 20) {
    alert("계측장소는 20자 이내여야 합니다");
    return false;
  } else if ($("#modInsPlace").val().length === 0) {
    alert("계측장소를 입력해주세요");
    return false;
  }

  if ($("#modContactName").val().length >= 20) {
    alert("이름은 20자 이내여야 합니다");
    return false;
  } else if ($("#modContactName").val().length === 0) {
    alert("이름을 입력해주세요");
    return false;
  }

  if ($("#modDepartment").val().length >= 20) {
    alert("근무부서는 20자 이내여야 합니다");
    return false;
  } else if ($("#modDepartment").val().length === 0) {
    alert("근무부서를 입력해주세요");
    return false;
  }

  if ($("#modMonitoringPlace").val().length >= 20) {
    alert("모니터링장소는 20자 이내여야 합니다");
    return false;
  } else if ($("#modMonitoringPlace").val().length === 0) {
    alert("모니터링장소를 입력해주세요");
    return false;
  }

  if ($("#modTesterSettingLoc").val().length >= 20) {
    alert("오염검사기는 20자 이내여야 합니다");
    return false;
  } else if ($("#modTesterSettingLoc").val().length === 0) {
    alert("오염검사기를 입력해주세요");
    return false;
  }

  if ($("#modOperEquipName").val().length === 0) {
    alert("장비를 선택해주세요");
    return false;
  } 

  if ($("#modDperEquipCount").val().length === 0) {
    alert("수량을 입력해주세요");
    return false;
  } 

  return true;
}