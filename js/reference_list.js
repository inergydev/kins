$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);

  objUserInfo = userInfo;

  fnVisibleElement("container", true);

  if (userInfo.USER_AUTH === "USER") {
    $("#btnDel").hide();
    $("#btnReg").hide();
  }

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);
  if (pagePrevUrl === "ref_reg") {
    let isPrevDataSync = localStorage.getItem("ref_reg");
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("ref_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });
  
  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });

  $("#btnReg").click(function(e){
    e.preventDefault();
    $(location).attr("href", "./reference_reg.html");
  });

  if (objUserInfo.USER_AUTH === "USER") {
    $("#ownTable > colgroup col:eq(0)").remove();
    $("#ownTable > thead th:eq(0)").remove();
  }
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("ref_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();

  if (searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val()
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/reference_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
      } else {
        fnDrawView(result.data);
        alert(result.errMsg);
      }
    }
  });
}

function fnCurrStateSave(obj) {
  localStorage.setItem("ref_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr>";
      if (objUserInfo.USER_AUTH !== "USER") {
        buildHtml = buildHtml + "<td><input type='checkbox' class='chk_info' id='chk_info" + aData.cid + "' value='" + aData.cid + "'><label for='chk_info" + aData.cid + "'><span class='blind'>선택</span></label></td>";
      }
      buildHtml = buildHtml + "<td>" + aData.no + "</td>";
      buildHtml = buildHtml + "<td class='title'><a href='javascript:void(0)' class='btn_view' value='" + aData.cid + "'>" + aData.title + "</a></td>";
      buildHtml = buildHtml + "<td><a href='javascript:void(0);' class='btn_mod btn_down' value1='" + aData.attachPath + "' value2='"  + aData.attachRealPath + "'>다운로드</a></td>"
      if (aData.registDate === null) {
        buildHtml = buildHtml + "<td>-</td>";
      } else {
        buildHtml = buildHtml + "<td>" + dateFormat("compact", aData.registDate.date) + "</td>";
      }
      buildHtml = buildHtml + "</tr>";
      $("#ownTable > tbody:last").append(buildHtml);
    }

    if (objUserInfo.USER_AUTH !== "USER") {
      // selectbox click event setting
      $("#chk_all").click(function(e){
        let chkInfos = $(".chk_info:checkbox").toArray();
        for (let i = 0; i < chkInfos.length; i++) {
          let aData = chkInfos[i];
          $(aData).prop("checked", $(this).is(":checked"));
        }
      });

      $("a.btn_down").click(function(e){
        let fileName = $(this).attr("value1");
        let fileRealPath = $(this).attr("value2");

        var x=new XMLHttpRequest();
        x.open( "GET", fileRealPath , true);
        x.responseType="blob";
        x.onload= function(e){download(e.target.response,fileName);};
        x.send();
      });

      $(".chk_info").click(function(e){
        let totalCount = $(".chk_info:checkbox").toArray().length;
        let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

        console.log(totalCount + " " + checkedCount);

        let chkAllCheckd = false;
        chkAllCheckd = (totalCount === checkedCount) ? true : false;
        $("#chk_all").prop("checked", chkAllCheckd);
      });

      $("a.btn_del").unbind("click");
      $("a.btn_del").click(function(e){
        e.preventDefault();
        let checkedData = $(".chk_info:checkbox:checked").toArray();
        if (checkedData.length === 0) {
          alert("삭제할 데이터를 선택해주세요");
          return;
        }

        if (confirm("삭제하시겠습니까?")) {
          let delArr = new Array();
          for (let i = 0; i < checkedData.length; i++) {
            let aData = checkedData[i];
            delArr.push(Number($(aData).val()));
          }
          
          console.log(delArr);
    
          let reqObj = {
            action : "delete",
            delArr : delArr
          };
      
          $.ajax({
            type: "POST",
            dataType: "json",
            url: "../php/reference_api.php",
            data: reqObj,
            success: function (result) {
              console.log(result);
              if(result.errCode === 0) {
                let movePage = pager.currPage;
    
                if (pager.currPage === 1) movePage = 1;
                else {
                  if (delArr.length === pager.defPagerPageSize) {
                    movePage = pager.currPage - 1;
                  }
                }
                fnGetData(movePage);
              } else {
                alert(result.errMsg);
              }
            }
          });
        }
      });
    };
    //Draw Pager
    fnDrawPager(data, fnGetData);
  } else {
    let span = 7;
    if (objUserInfo.USER_AUTH === "USER") span = 6;
    $("#ownTable > tbody:last").append("<tr><td colspan='" + span + "'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}