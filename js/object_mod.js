$(document).ready(function(){
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  let cid = localStorage.getItem("obj_mod");

  if (cid === null || cid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./object_list.html");
      return;
    }
  }

  $("#modBtn").click(function(e){
    fnModData(cid);
  });

  $("#cancelBtn").click(function(e){
    localStorage.setItem("obj_reg", false);
    $(location).attr("href", "./object_list.html");
  });

  $("#modEntryYear").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryMonth").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryDay").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
  
  $("#modEntryHour").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modEntryMinute").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modAge").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#modSex").append("<option value='' selected disabled>선택</option>");
  let types = fnGetCode("objSex");
  for (let i = 0; i < types.length; i++) {
    let aData = types[i];
    let html = "<option value='" + aData + "'>" + aData + "</option>";
    $("#modSex").append(html);
  }

  fnGetData(cid);
}

function fnGetData(cid) {
  let reqObj = {
    action : "aSelect",
    cid : cid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/object_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            $(location).attr("href", "./object_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnDrawView(data) {
  $("#modName").val(data.info.name);
  $("#modCountry").val(data.info.country);
  $("#modAge").val(data.info.age);
  $("#modSex").val(data.info.sex);

  if (data.info.entry_date !== null) {
    let dateString = dateFormat("full", data.info.entry_date.date);
    dateStringOne = dateString.substring(0, 10);
    let dateArr = dateStringOne.split("-");
    $("#modEntryYear").val(dateArr[0]);
    $("#modEntryMonth").val(dateArr[1]);
    $("#modEntryDay").val(dateArr[2]);

    dateStringTwo = dateString.substring(11, 16);
    dateArr = dateStringTwo.split(":");
    $("#modEntryHour").val(dateArr[0]);
    $("#modEntryMinute").val(dateArr[1]);
  }

  $("#modVehicleInfo").val(data.info.vehicle_info);
  $("#modDeparturePlace").val(data.info.departure_place);
  $("#modContaminantDetail").val(data.info.contaminant_detail);
}

function fnModData(cid) {
  if(!fnValidationCheck()) return;

  let dateString = $("#modEntryYear").val() + "-" + pad($("#modEntryMonth").val(), 2) + "-" +  pad($("#modEntryDay").val(), 2) +  "-" + pad($("#modEntryHour").val(), 2) +  "-" + pad($("#modEntryMinute").val(), 2);

  console.log(dateString);

  if(confirm("수정하시겠습니까?")) {
    let reqObj = {
      action : "update",
      cid : cid,
      name : $("#modName").val(),
      country : $("#modCountry").val(),
      age : Number($("#modAge").val()),
      sex : $("#modSex").val(),
      date : dateString,
      vehicleInfo : $("#modVehicleInfo").val(),
      departurePlace : $("#modDeparturePlace").val(),
      contaminantDetail : $("#modContaminantDetail").val()
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/object_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./object_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}

function fnValidationCheck() {
  if ($("#modName").val().length >= 20) {
    alert("이름은 20자 이내여야 합니다");
    return false;
  } else if ($("#modName").val().length === 0) {
    alert("이름를 입력해주세요");
    return false;
  }

  if ($("#modCountry").val().length >= 20) {
    alert("국적은 20자 이내여야 합니다");
    return false;
  } else if ($("#modCountry").val().length === 0) {
    alert("국적을 입력해주세요");
    return false;
  }

  if ($("#modAge").val().length === 0) {
    alert("나이를 입력해주세요");
    return false;
  }

  if ($("#modAge").val() === null) {
    alert("성별을 선택해주세요.");
    return false;
  }

  let dateString = $("#modEntryYear").val() + pad($("#modEntryMonth").val(), 2) + pad($("#modEntryDay").val(), 2);
  if (!fnDateCheck(dateString)) {
    alert("날짜의 형식이 올바르지 않습니다.");
    return false;
  }

  let hour = Number($("#modEntryHour").val());
  let minute = Number($("#modEntryMinute").val());
  
  if ($("#modEntryHour").val().length === 0) {
    alert("시간을 입력해주세요.");
    return false;
  }

  if (hour > 24) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modEntryMinute").val().length === 0) {
    alert("분을 입력해주세요.");
    return false;
  }

  if (minute > 59) {
    alert("분의 형식이 올바르지 않습니다.");
    return false;
  }

  if ($("#modVehicleInfo").val().length >= 30) {
    alert("여객정보는 30자 이내여야 합니다");
    return false;
  } else if ($("#modVehicleInfo").val().length === 0) {
    alert("여객정보를 입력해주세요");
    return false;
  }

  if ($("#modDeparturePlace").val().length >= 30) {
    alert("출발지는 30자 이내여야 합니다");
    return false;
  } else if ($("#modDeparturePlace").val().length === 0) {
    alert("출발지를 입력해주세요");
    return false;
  }

  if ($("#modContaminantDetail").val().length === 0) {
    alert("오염 상세정보입력해주세요");
    return false;
  }

  return true;
}