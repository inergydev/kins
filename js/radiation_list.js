$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

objUserInfo = {};

function fnCheckSessionCallback(userInfo) {
  console.log(userInfo);
  fnVisibleElement("container", true);
  objUserInfo = userInfo;

  let pagePrevUrl = fnParseUrlFileName(gPagePrevUrl);
  if (pagePrevUrl === "radiation_mod") fnPrevDataSet();
  else if (pagePrevUrl === "radiation_reg") {
    let isPrevDataSync = localStorage.getItem("rad_reg");
    console.log(isPrevDataSync);
    if (isPrevDataSync === "true" ) {
      localStorage.getItem("rad_reg", false);
      fnGetData(1);
    } else {
      fnPrevDataSet();
    }
  } else fnGetData(1);

  $("#searchBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $("#searchOptionBtn").click(function(e){
    if (!fnSearchValidationCheck()) return;

    fnGetData(1);
  });

  $(".search_area").keyup(function(e) {
    if (e.keyCode == 13) $("#searchBtn").click();
  });

  $("#listAccidentDate").click(function(e){
    let checked = $("#listAccidentDate").is(":checked");
    $("#listAccidentStartDate").prop('disabled', !checked);
    $("#listAccidentEndDate").prop('disabled', !checked);
  });

  $("#listAccidentStartDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });

  $("#listAccidentEndDate").keyup(function(e){
    $(this).val($(this).val().replace(/[^0-9]/g,""));
  });
}

function fnPrevDataSet() {
  console.log("mod/reg > list :: list sync");
  // Draw Previous State
  let obj = JSON.parse(localStorage.getItem("rad_list"));

  if (obj === null) {
    fnGetData(1);
  } else {
    $("#searchKeyword").val(obj.searchKeyword)
    $("#listCountry").prop("checked", false);
    $("#listAccidentLoc").prop("checked", false);
    $("#listAccidentFacility").prop("checked", false);
    $("#listAccidentLevel").prop("checked", false);
    $("#listAccidentKinsLevel").prop("checked", false);
    $("#listAccidentDate").prop("checked", false);

    for (let i = 0; i < obj.searchOptions.length; i++) {
      let aData = obj.searchOptions[i];
      if (aData === "country") $("#listCountry").prop("checked", true);
      if (aData === "accident_loc") $("#listAccidentLoc").prop("checked", true);
      if (aData === "accident_facility") $("#listAccidentFacility").prop("checked", true);
      if (aData === "accident_level") $("#listAccidentLevel").prop("checked", true);
      if (aData === "accident_kins_level") $("#listAccidentKinsLevel").prop("checked", true);
    }

    if (obj.searchDateOption) {
      $("#listAccidentDate").prop("checked", true);
      $("#listAccidentStartDate").prop("disabled", false);
      $("#listAccidentEndDate").prop("disabled", false);
      $("#listAccidentStartDate").val(obj.dateStart.replace(/-/gi, ""));
      $("#listAccidentEndDate").val(obj.dateEnd.replace(/-/gi, ""));
    }
    fnGetData(obj.page);
  }
}

function fnSearchValidationCheck() {
  let searchKeyword = $("#searchKeyword").val();
  let option_1 = $("#listCountry").is(":checked");
  let option_2 = $("#listAccidentLoc").is(":checked");
  let option_3 = $("#listAccidentFacility").is(":checked");
  let option_4 = $("#listAccidentLevel").is(":checked");
  let option_5 = $("#listAccidentKinsLevel").is(":checked");
  let option_6 = $("#listAccidentDate").is(":checked");

  if (!option_1 && !option_2 && !option_3 && !option_4 && !option_5 && !option_6) {
    alert("검색옵션을 1개 이상 선택해주세요");
    return false;
  }

  if ((option_1 || option_2 || option_3 || option_4 || option_5) && searchKeyword.length === 0) {
    alert("검색어를 입력해주세요");
    return false;
  }

  return true;
}

function fnGetData(page) {
  if(!fnValidationCheck()) return;

  let searchOptions = new Array();
  if ($("#listCountry").is(":checked")) searchOptions.push("country");
  if ($("#listAccidentLoc").is(":checked")) searchOptions.push("accident_loc");
  if ($("#listAccidentFacility").is(":checked")) searchOptions.push("accident_facility");
  if ($("#listAccidentLevel").is(":checked")) searchOptions.push("accident_level");
  if ($("#listAccidentKinsLevel").is(":checked")) searchOptions.push("accident_kins_level");
  let dateStartString = $("#listAccidentStartDate").val();
  let dateEndString = $("#listAccidentEndDate").val();
  let isSearchDateOption = false;
  if ($("#listAccidentDate").is(":checked")) {
    isSearchDateOption = true;
    dateStartString = dateStartString.substring(0, 4) + "-" + dateStartString.substring(4, 6) + "-" + dateStartString.substring(6, 8);
    dateEndString = dateEndString.substring(0, 4) + "-" + dateEndString.substring(4, 6) + "-" + dateEndString.substring(6, 8);
  }

  let reqObj = {
    action : "select",
    page : page,
    searchKeyword : $("#searchKeyword").val(),
    searchOptions : searchOptions,
    searchDateOption : isSearchDateOption,
    dateStart : dateStartString,
    dateEnd : dateEndString,
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/radiation_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        fnCurrStateSave(reqObj);
        fnDrawView(result.data);
        fnVisibleElement("container", true);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnValidationCheck() {
  if ($("#listAccidentDate").is(":checked")) {
    let dateStartString = $("#listAccidentStartDate").val();
    let dateEndString = $("#listAccidentEndDate").val();

    if (!fnDateCheck(dateStartString)) {
      alert("사고 일시 '시작' 날짜형식이 올바르지 않습니다");
      return false;
    }
    if (!fnDateCheck(dateEndString)) {
      alert("사고 일시 '종료' 날짜형식이 올바르지 않습니다");
      return false;
    }
  }
  return true;
}

function fnCurrStateSave(obj) {
  localStorage.setItem("rad_list", JSON.stringify(obj));
}

function fnDrawView(data) {
  // Remove tbody
  $("#ownTable > tbody").html("");
  $("#pager").html("");
  $("#chk_all").prop("checked", false);

  // draw tbody
  let listArr = data.listArr;
  let pager = data.pager;

  if (listArr.length > 0) {
    
    for (let i = 0; i < listArr.length; i++) {
      let aData = listArr[i];
      let buildHtml = "<tr>";
      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td></td>";
      } else {
        buildHtml = buildHtml + "<td><input type='checkbox' class='chk_info' id='chk_info" + aData.cid + "' value='" + aData.cid + "'><label for='chk_info" + aData.cid + "'><span class='blind'>선택</span></label></td>";
      }
      buildHtml = buildHtml + "<td>" + aData.no + "</td>";
      buildHtml = buildHtml + "<td>" + aData.country + "</td>";
      buildHtml = buildHtml + "<td>" + aData.accident_loc + "</td>";
      buildHtml = buildHtml + "<td>" + aData.accident_facility + "</td>";

      if (aData.accident_date === null) {
        buildHtml = buildHtml + "<td>-</td>";
      } else {
        buildHtml = buildHtml + "<td>" + dateFormat("compact", aData.accident_date.date) + "</td>";
      }    

      buildHtml = buildHtml + "<td>" + aData.accident_level + "</td>";
      buildHtml = buildHtml + "<td>" + aData.accident_kins_level + "</td>";

      if (objUserInfo.USER_AUTH === "USER" && objUserInfo.UID !== aData.uid) {
        buildHtml = buildHtml + "<td><button type='button' class='btn_mod btn_disable' disabled>수정</button></td>";
      } else {
        buildHtml = buildHtml + "<td><button type='button' class='btn_mod' value='" + aData.cid + "'>수정</button></td>";
      }
      buildHtml = buildHtml + "</tr>";

      $("#ownTable > tbody:last").append(buildHtml);
    }

    // selectbox click event setting
    $("#chk_all").click(function(e){      
      let chkInfos = $(".chk_info:checkbox").toArray();
      for (let i = 0; i < chkInfos.length; i++) {
        let aData = chkInfos[i];
        $(aData).prop("checked", $(this).is(":checked"));
      }
    });

    $(".chk_info").click(function(e){
      let totalCount = $(".chk_info:checkbox").toArray().length;
      let checkedCount = $(".chk_info:checkbox:checked").toArray().length;

      console.log(totalCount + " " + checkedCount);

      let chkAllCheckd = false;
      chkAllCheckd = (totalCount === checkedCount) ? true : false;
      $("#chk_all").prop("checked", chkAllCheckd);
    });


    $(".btn_mod").click(function(e){
      e.preventDefault();
      localStorage.setItem("rad_mod", $(this).val());
      $(location).attr("href", "./radiation_mod.html");
    });

    $("a.btn_del").unbind("click");
    $("a.btn_del").click(function(e){
      e.preventDefault();
      let checkedData = $(".chk_info:checkbox:checked").toArray();
      if (checkedData.length === 0) {
        alert("삭제할 데이터를 선택해주세요");
        return;
      }

      if (confirm("삭제하시겠습니까?")) {
        let delArr = new Array();
        for (let i = 0; i < checkedData.length; i++) {
          let aData = checkedData[i];
          delArr.push(Number($(aData).val()));
        }
        
        console.log(delArr);
  
        let reqObj = {
          action : "delete",
          delArr : delArr
        };
    
        $.ajax({
          type: "POST",
          dataType: "json",
          url: "../php/radiation_api.php",
          data: reqObj,
          success: function (result) {
            console.log(result);
            if(result.errCode === 0) {
              let movePage = pager.currPage;
              if (pager.currPage === 1) movePage = 1;
              else {
                if (delArr.length === pager.defPagerPageSize) {
                  movePage = pager.currPage - 1;
                }
              }
              fnGetData(movePage);
            } else {
              alert(result.errMsg);
            }
          }
        });
      }
    });

    // Draw Pager
    fnDrawPager(data, fnGetData);
  } else {
    $("#ownTable > tbody:last").append("<tr><td colspan='9'><div class='non_data'>데이터가 없습니다.</div></td></tr>");
  }
}
