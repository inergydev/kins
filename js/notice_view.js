$(document).ready(function(){
  fnVisibleElement("container", false);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(userInfo) {
  let cid = localStorage.getItem("notice_view");

  console.log(cid);

  if (cid === null || cid === undefined) {
    if (!alert("정보가 없습니다")) {
      $(location).attr("href", "./notice_list.html");
      return;
    }
  }

  if (userInfo.USER_AUTH === "USER") {
    $("#btnDel").hide();
    $("#btnMod").hide();
  }

  fnGetData(cid);
}

function fnGetData(cid) {
  let reqObj = {
    action : "aSelect",
    cid : cid
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/notice_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      if(result.errCode === 0) {
        if (result.data.info !== null) {
          fnDrawView(result.data);
          fnVisibleElement("container", true);
        } else {
          if (!alert("정보가 없습니다")) {
            // $(location).attr("href", "./member_list.html");
            return;
          }
        }
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnDrawView(data) {
  $("#noticeTitle").append(data.info.title);
  $("#noticeRegDate").append(dateFormat("compact", data.info.regist_date.date));

  if (data.info.attach_r_name !== null && data.info.attach_r_name.length > 0) {
    console.log()
    $("#noticeAttach").append("<a href='javascript:void(0);' id='attachFile' value1='" + data.info.attachPath + "' value2='"  + data.info.attachRealPath + "'>" + data.info.attach_r_name);
  } else {
    $("#noticeAttach").append("");
  }
  $("#noticeContent").append("<pre>" + data.info.contents + "</pre>");

  $("#attachFile").click(function(e){
    let fileName = $(this).attr("value1");
    let fileRealPath = $(this).attr("value2");

    var x=new XMLHttpRequest();
    x.open( "GET", fileRealPath , true);
    x.responseType="blob";
    x.onload= function(e){download(e.target.response,fileName);};
    x.send();
  });

  $("#btnDel").click(function(e){
    e.preventDefault();
    fnDeleteData();
  });

}

function fnDeleteData() {
  if (confirm("삭제하시겠습니까?")) {
    let cid = Number(localStorage.getItem("notice_view"));

    let reqObj = {
      action : "delete",
      delArr : [cid]
    };

    $.ajax({
      type: "POST",
      dataType: "json",
      url: "../php/notice_api.php",
      data: reqObj,
      success: function (result) {
        console.log(result);
        if(result.errCode === 0) {
          $(location).attr("href", "./notice_list.html");
        } else {
          alert(result.errMsg);
        }
      }
    });
  }
}


