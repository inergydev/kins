$(document).ready(function(){
  fnVisibleElement("container", true);
  fnCheckSession(false, "./login.html", fnCheckSessionCallback);
});

function fnCheckSessionCallback(data) {
  fnGetData(1);
}

function fnGetData(page) {
  let reqObj = {
    action : "select",
    top : 3
  };

  $.ajax({
    type: "POST",
    dataType: "json",
    url: "../php/main_api.php",
    data: reqObj,
    success: function (result) {
      console.log(result);
      fnVisibleElement("container", true);
      if(result.errCode === 0) {
        fnDrawView(result.data);
      } else {
        alert(result.errMsg);
      }
    }
  });
}

function fnDrawView(data) {
  let notiListArr = data.notiListArr;
  let refListArr = data.refListArr;

  console.log(data);
  
  if (notiListArr.length > 0) {
    for (let i = 0; i < notiListArr.length; i++) {
      let aData = notiListArr[i];
      let strArr = aData.registDate.date.split(".");
      let parseDateString = strArr[0].replace(/-/gi, "/");
      let date = new Date(parseDateString);

      let year = date.getFullYear().toString().substr(-2);;
      let month = pad(date.getMonth() + 1, 2);
      let day = pad(date.getDate(), 2);

      $("#noticeList").append("<li><span class='date'><strong>" + day + "</strong>" + year + "." + month + "</span><span><a href='javascript:void(0)' class='noticeDetail' value='" + aData.cid + "'>" + aData.title + "</a></span></li>");
    }

    $("a.noticeDetail").click(function(e){
      e.preventDefault();
      let cid = $(this).attr("value");
      console.log(cid);
      localStorage.setItem("notice_view", $(this).attr("value"));
      $(location).attr("href", "./notice_view.html");
    });

  } else {
    $("#noticeList").append("<li><br>데이터가 없습니다</li>");
  }

  if (refListArr.length > 0) {
    for (let i = 0; i < refListArr.length; i++) {
      let aData = refListArr[i];
      let strArr = aData.registDate.date.split(".");
      let parseDateString = strArr[0].replace(/-/gi, "/");
      let date = new Date(parseDateString);

      let year = date.getFullYear().toString().substr(-2);;
      let month = pad(date.getMonth() + 1, 2);
      let day = pad(date.getDate(), 2);

      // $("#refList").append("<li><span class='date'><strong>" + day + "</strong>" + year + "." + month + "</span><span><a href='javascript:void(0)' class='refDetail' value='" + aData.cid + "'>" + aData.title + "</a></span></li>");;
      $("#refList").append("<li><span class='date'><strong>" + day + "</strong>" + year + "." + month + "</span><span><a href='javascript:void(0);' id='attachFile' class='refDetail'  value1='" + aData.attachPath + "' value2='" + aData.attachRealPath + "'>" + aData.title + "</a></span></li>");
    }

    $(".refDetail").click(function(e){
      let fileName = $(this).attr("value1");
      let fileRealPath = $(this).attr("value2");

      console.log(fileName);
      console.log(fileRealPath);

      var x=new XMLHttpRequest();
      x.open( "GET", fileRealPath , true);
      x.responseType="blob";
      x.onload= function(e){download(e.target.response,fileName);};
      x.send();
    });


    // 자료실은 뷰가 없음
    // $("a.refDetail").click(function(e){
    //   e.preventDefault();
    //   let cid = $(this).attr("value");
    //   console.log(cid);
    //   localStorage.setItem("ref_view", $(this).attr("value"));
    //   $(location).attr("href", "./reference_view.html");
    // });

  } else {
    $("#refList").append("<li><br>데이터가 없습니다</li>");
  }


  
}