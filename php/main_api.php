<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");

  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();

  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  // Action
  switch($reqObj->action) {
    case "select" :
      // Query
      $sql = "SELECT * FROM KINSDB.dbo.tb_noti";
      $sql = "SELECT * FROM (";
      $sql .= "select ROW_NUMBER() OVER (ORDER BY regist_date Desc) AS 'RowNumber', * From tb_noti";
      $sql .= ") a WHERE RowNumber BETWEEN 0 AND ".$reqObj->top;

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      $notiListArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $aData = new stdClass();
          $aData->cid = $row['cid'];
          $aData->uid = $row['uid'];
          $aData->title = urldecode($row['title']);
          $aData->registDate = $row['regist_date'];
          $aData->updateDate = $row['update_date'];
          $notiListArr[] = $aData;
          unset($aData);
        }
      }
      // Data Setting
      $rstObj->data["notiListArr"] = $notiListArr;

      // Query
      $sql = "SELECT * FROM KINSDB.dbo.tb_ref";
      $sql = "SELECT * FROM (";
      $sql .= "select ROW_NUMBER() OVER (ORDER BY regist_date Desc) AS 'RowNumber', * From tb_ref";
      $sql .= ") a WHERE RowNumber BETWEEN 0 AND ".$reqObj->top;

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      $refListArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $aData = new stdClass();
          $aData->cid = $row['cid'];
          $aData->uid = $row['uid'];
          $aData->title = urldecode($row['title']);
          $aData->attachPath = $row['attach_r_name'];
          $aData->attachRealPath = DEF_FILE_REF_DOWNLOAD_PATH.$row['attach_v_name'];
          $aData->registDate = $row['regist_date'];
          $aData->updateDate = $row['update_date'];
          $refListArr[] = $aData;
          unset($aData);
        }
      }
      // Data Setting
      $rstObj->data["refListArr"] = $refListArr;

      // JSON return
      echo json_encode($rstObj);
      break;
    case "delete" : 
      break;
    case "aSelect" : 
      break;
    case "update" : 
      break;
    case "insert" : 
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>