<?php
  // env
  if (!defined('DEF_ENV')) define("DEF_ENV", "TB"); // LOCAL, TB, CB

  if (!strcmp(DEF_ENV, "LOCAL")) {
    // Base Path
    if (!defined('DEF_BASE_PATH')) define("DEF_BASE_PATH", "http://localhost:3000");

    // DB Connection
    if (!defined('DEF_DB_HOST')) define("DEF_DB_HOST", "192.168.0.41");
    if (!defined('DEF_DB_NAME')) define("DEF_DB_NAME", "KINSDB");
    if (!defined('DEF_DB_USER')) define("DEF_DB_USER", "sa");
    if (!defined('DEF_DB_PASS')) define("DEF_DB_PASS", "inergy1234");
    if (!defined('DEF_DB_CHAR')) define("DEF_DB_CHAR", "UTF-8");
  
    // File
    if (!defined('DEF_FILE_NOTICE_UPLOAD_PATH')) define("DEF_FILE_NOTICE_UPLOAD_PATH", "../upload/notice/");
    if (!defined('DEF_FILE_REF_UPLOAD_PATH')) define("DEF_FILE_REF_UPLOAD_PATH", "../upload/reference/");
  } else if (!strcmp(DEF_ENV, "TB")) {
    // Base Path
    if (!defined('DEF_BASE_PATH')) define("DEF_BASE_PATH", "http://211.170.81.111:15500");

    // DB Connection
    if (!defined('DEF_DB_HOST')) define("DEF_DB_HOST", "192.168.0.41");
    if (!defined('DEF_DB_NAME')) define("DEF_DB_NAME", "KINSDB");
    if (!defined('DEF_DB_USER')) define("DEF_DB_USER", "sa");
    if (!defined('DEF_DB_PASS')) define("DEF_DB_PASS", "inergy1234");
    if (!defined('DEF_DB_CHAR')) define("DEF_DB_CHAR", "UTF-8");

    // File
    if (!defined('DEF_FILE_NOTICE_UPLOAD_PATH')) define("DEF_FILE_NOTICE_UPLOAD_PATH", "C:\\KINS\\upload\\notice\\");
    if (!defined('DEF_FILE_REF_UPLOAD_PATH')) define("DEF_FILE_REF_UPLOAD_PATH", "C:\\KINS\\upload\\reference\\");
  } else if (!strcmp(DEF_ENV, "CB")) {
    // Base Path
    if (!defined('DEF_BASE_PATH')) define("DEF_BASE_PATH", "");
    // DB Connection
    if (!defined('DEF_DB_HOST')) define("DEF_DB_HOST", "");
    if (!defined('DEF_DB_NAME')) define("DEF_DB_NAME", "");
    if (!defined('DEF_DB_USER')) define("DEF_DB_USER", "");
    if (!defined('DEF_DB_PASS')) define("DEF_DB_PASS", "");
    if (!defined('DEF_DB_CHAR')) define("DEF_DB_CHAR", "");

    // File
    if (!defined('DEF_FILE_NOTICE_UPLOAD_PATH')) define("DEF_FILE_NOTICE_UPLOAD_PATH", "C:\\KINS\\upload\\notice\\");
    if (!defined('DEF_FILE_REF_UPLOAD_PATH')) define("DEF_FILE_REF_UPLOAD_PATH", "C:\\KINS\\upload\\reference\\");
  } 

  // File
  if (!defined('DEF_FILE_NOTICE_DOWNLOAD_PATH')) define("DEF_FILE_NOTICE_DOWNLOAD_PATH", DEF_BASE_PATH."/upload/notice/");
  if (!defined('DEF_FILE_REF_DOWNLOAD_PATH')) define("DEF_FILE_REF_DOWNLOAD_PATH", DEF_BASE_PATH."/upload/reference/");
  
  // Session
  if (!defined('DEF_SESSION_TIMEOUT')) define("DEF_SESSION_TIMEOUT", 30 * (24*60*60));
  // if (!defined('DEF_SESSION_TIMEOUT')) define("DEF_SESSION_TIMEOUT", 30);

  // Logger
  if (!defined('DEF_DEBUG_LOG_FILE_PATH')) define("DEF_DEBUG_LOG_FILE_PATH", "../log/debug.log");

  // Pager
  if (!defined('DEF_PAGER_BLOCK_SIZE')) define("DEF_PAGER_BLOCK_SIZE", 10);
  if (!defined('DEF_PAGER_PAGE_SIZE')) define("DEF_PAGER_PAGE_SIZE", 10);


?>