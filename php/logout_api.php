<?php
  include ("./define.php");
  
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // endSession();
  session_start([
    'cookie_lifetime' => DEF_SESSION_TIMEOUT
  ]);
  unset($_SESSION["INFO"]);
  session_destroy();

  // JSON return
  echo json_encode($rstObj);
?>
