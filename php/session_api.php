<?php
  include_once ("./define.php");
  include_once ("./logger.php");

  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  if (!strcmp($reqObj->type, "checkSession")) {
    checkSession();
  } else if (!strcmp($reqObj->type, "getUserInfo")) {
    getUserInfo();
  }

  function checkSession() {
    global $rstObj;

    session_start([
      'cookie_lifetime' => DEF_SESSION_TIMEOUT
    ]);
    if (is_null($_SESSION["INFO"]["SESSIONID"])) {
      $rstObj->errCode = 3;
      $rstObj->errMsg = "Session Invalid.";
      session_destroy();
    } else {
    }
    
    $rstObj->userInfoObj = $_SESSION["INFO"];

    echo json_encode($rstObj);
  }

  function getSessoinInfo() {
    session_start([
      'cookie_lifetime' => DEF_SESSION_TIMEOUT
    ]);
    return $_SESSION["INFO"];
  }

  function getUserInfo() {
    global $rstObj;
    
    session_start([
      'cookie_lifetime' => DEF_SESSION_TIMEOUT
    ]);
    $rstObj->userInfoObj = $_SESSION["INFO"];
    
    echo json_encode($rstObj);
  }
?>