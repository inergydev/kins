<?php
  include_once ("./define.php");
  ini_set("error_reporting", E_ALL);
  ini_set("error_log", DEF_DEBUG_LOG_FILE_PATH);

  function debug($obj) {
    error_log(print_r("> DEBUG ======== ", true), 3, DEF_DEBUG_LOG_FILE_PATH);
    error_log(print_r($obj, TRUE), 3, DEF_DEBUG_LOG_FILE_PATH);
    error_log(print_r("\r\n", true), 3, DEF_DEBUG_LOG_FILE_PATH);
  }
?>