<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");
  include_once ("./session_util.php");
  
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  // Action
  switch($reqObj->action) {
    case "select" : 
      // Query
      $aSession = getSessoinInfo();
      $sql = "SELECT ROW_NUMBER() OVER(ORDER BY regist_date ASC) AS no, * FROM KINSDB.dbo.tb_user";

      debug($aSession);

      if (!strcmp($aSession["USER_AUTH"], "SUPER")) {
        if (!strcmp($reqObj->searchType, "ADMIN")) {
          $sql .= " WHERE user_auth IN ('ADMIN')";
        } else if (!strcmp($reqObj->searchType, "USER")){
          $sql .= " WHERE user_auth IN ('USER')";
        } else {
          $sql .= " WHERE user_auth NOT IN ('SUPER')";
        }
      } else if (!strcmp($aSession["USER_AUTH"], "ADMIN")) {
        $sql .= " WHERE user_auth = 'USER'";
      }

      if(isset($reqObj->searchOptions))
      {
        $sql .= " AND (";
        for ($i = 0; $i < count($reqObj->searchOptions); $i++) {
          $aData = $reqObj->searchOptions[$i];
          $sql .= $aData." LIKE '%".$reqObj->searchKeyword."%'";

          if ($i != count($reqObj->searchOptions) - 1) {
            $sql .= " OR ";
          }
        }
        $sql .= ")";
      }

      debug($sql);

      // Pager Setting
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );

      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
        echo json_encode($rstObj);
        return;
      }

      $pager["totalCount"] = sqlsrv_num_rows($result);
      $pager["currPage"] = (int)$reqObj->page;
      $pager["defPagerPageSize"] = DEF_PAGER_PAGE_SIZE;
      $pager["defPagerBlockSize"] = DEF_PAGER_BLOCK_SIZE;

      // Real Data Select
      $offset = ($reqObj->page * DEF_PAGER_PAGE_SIZE) - DEF_PAGER_PAGE_SIZE;
      $sql .=" ORDER BY regist_date DESC";
      $sql .=" OFFSET ".$offset." ROWS FETCH NEXT ".DEF_PAGER_PAGE_SIZE." ROWS ONLY";
      
      $result = sqlsrv_query( $conn, $sql );
      $listArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $aData = new stdClass();
          $aData->no = $row['no'];
          $aData->uid = $row['uid'];
          $aData->id = $row['id'];
          $aData->orgName = $row['org_name'];
          $aData->contactName = $row['contact_name'];
          $aData->rank = $row['rank'];
          $aData->userAuth = $row['user_auth'];
          $aData->registDate = $row['regist_date'];
          $aData->lastAccessDate = $row['last_access_date'];
          $listArr[] = $aData;
          unset($aData);
        }
      }

      // Data Setting
      $rstObj->data["pager"] = $pager;
      $rstObj->data["listArr"] = $listArr;

      // JSON return
      echo json_encode($rstObj);

      break;
    case "delete" :
      debug($reqObj->delArr);
      $sql = "DELETE FROM KINSDB.dbo.tb_user";
      $sql .= " WHERE uid IN(";

      for($i = 0; $i < count($reqObj->delArr); $i++) {
        $aData = $reqObj->delArr[$i];
        $sql .= $aData;

        if ($i < count($reqObj->delArr) - 1) {
          $sql .= ", ";
        }

        unset($aData);
      }

      $sql .= ")";
      debug($sql);

      $result = sqlsrv_query( $conn, $sql );

      // JSON return
      echo json_encode($rstObj);
      break;
    case "aSelect" :
      $sql = "SELECT * FROM KINSDB.dbo.tb_user WHERE uid=".$reqObj->uid;
      $result = sqlsrv_query( $conn, $sql );
      
      $listArr = array();
      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      $rstObj->data["info"] = $listArr[0];

      // JSON return
      echo json_encode($rstObj);
      break;
    case "update" :
      debug($reqObj);

      // id 중복체크
      $sql = "SELECT id FROM KINSDB.dbo.tb_user WHERE uid != ".$reqObj->uid." AND id = '".$reqObj->id."'";
      debug($sql);
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );
      if (sqlsrv_num_rows($result) > 0) {
        $rstObj->errCode = 2;
        $rstObj->errMsg = "Duplicated Id";
        echo json_encode($rstObj);
      } else {
        $sql = "UPDATE KINSDB.dbo.tb_user";
        $sql .= " SET";
        $sql .=" org_name = N'".strip_tags($reqObj->orgName)."',";
        $sql .=" contact_name = N'".strip_tags($reqObj->contactName)."',";
        $sql .=" rank = N'".strip_tags($reqObj->rank)."',";
        $sql .=" id = '".strip_tags($reqObj->id)."',";
        $sql .=" pwd = '".strip_tags($reqObj->pwd)."',";
        $sql .=" use_yn = '".$reqObj->useYn."',";
        $sql .=" user_auth = '".$reqObj->userAuth."'";
        $sql .=" WHERE uid=".$reqObj->uid;

        $result = sqlsrv_query( $conn, $sql );
        if ($result == false) {
          dbFormatErrors(sqlsrv_errors());
          $rstObj->errCode = 99;
          $rstObj->errMsg = "DB Precess Error";
        }
        echo json_encode($rstObj);
      }
      break;
    case "insert" :
      debug($reqObj);

      // id 중복체크
      $sql = "SELECT id FROM KINSDB.dbo.tb_user WHERE id = '".$reqObj->id."'";
      debug($sql);
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );
      if (sqlsrv_num_rows($result) > 0) {
        $rstObj->errCode = 2;
        $rstObj->errMsg = "Duplicated Id";
        echo json_encode($rstObj);
      } else {
        $sql = "INSERT INTO KINSDB.dbo.tb_user (org_name, contact_name, rank, id, pwd, use_yn, user_auth) ";
        $sql .= " VALUES(";
        $sql .= " N'".strip_tags($reqObj->orgName)."',";
        $sql .= " N'".strip_tags($reqObj->contactName)."',";
        $sql .= " N'".strip_tags($reqObj->rank)."',";
        $sql .= " '".$reqObj->id."',";
        $sql .= " '".$reqObj->pwd."',";
        $sql .= " '".$reqObj->useYn."',";
        $sql .= " '".$reqObj->userAuth."'";
        $sql .= ")";

        debug($sql);

        $result = sqlsrv_query( $conn, $sql );
        if ($result == false) {
          dbFormatErrors(sqlsrv_errors());
          $rstObj->errCode = 99;
          $rstObj->errMsg = "DB Precess Error";
        }
        echo json_encode($rstObj);
      }
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>