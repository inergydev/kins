<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");
  
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  debug($reqObj);

  // Action
  switch($reqObj->action) {
    case "select" : 
      // Query
      $sql = "SELECT ROW_NUMBER() OVER(ORDER BY regist_date ASC) AS no, * FROM KINSDB.dbo.tb_rad";

      $isDateSearch = false;
      if(isset($reqObj->searchOptions))
      {
        $sql .= " WHERE (";
        for ($i = 0; $i < count($reqObj->searchOptions); $i++) {
          $aData = $reqObj->searchOptions[$i];
          $sql .= $aData." LIKE '%".$reqObj->searchKeyword."%'";

          if ($i != count($reqObj->searchOptions) - 1) {
            $sql .= " OR ";
          }
        }
        $sql .= ")";
      }

      if(!strcmp($reqObj->searchDateOption, "true")) {
        $dateStartArr = explode("-", $reqObj->dateStart);
        $dateStart = new DateTime();
        $dateStart->setDate($dateStartArr[0], $dateStartArr[1], $dateStartArr[2]);
        $dateStart->setTime(0, 0, 0);

        $dateEndArr = explode("-", $reqObj->dateEnd);
        $dateEnd = new DateTime();
        $dateEnd->setDate($dateEndArr[0], $dateEndArr[1], $dateEndArr[2]);
        $dateEnd->setTime(23, 59, 59);

        if (isset($reqObj->searchOptions)) $sql .= " AND";
        else $sql .= " WHERE";

        $sql .= " accident_date BETWEEN '".$dateStart->format("Y-m-d H:i:s")."' AND '".$dateEnd->format("Y-m-d H:i:s")."'";
      }

      debug($sql);

      // Pager Setting
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );

      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
        echo json_encode($rstObj);
        return;
      }

      $pager["totalCount"] = sqlsrv_num_rows($result);
      $pager["currPage"] = (int)$reqObj->page;
      $pager["defPagerPageSize"] = DEF_PAGER_PAGE_SIZE;
      $pager["defPagerBlockSize"] = DEF_PAGER_BLOCK_SIZE;

      // Real Data Select
      $offset = ($reqObj->page * DEF_PAGER_PAGE_SIZE) - DEF_PAGER_PAGE_SIZE;
      $sql .=" ORDER BY regist_date DESC";
      $sql .=" OFFSET ".$offset." ROWS FETCH NEXT ".DEF_PAGER_PAGE_SIZE." ROWS ONLY";
      
      $result = sqlsrv_query( $conn, $sql );
      $listArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      // Data Setting
      $rstObj->data["pager"] = $pager;
      $rstObj->data["listArr"] = $listArr;

      // JSON return
      echo json_encode($rstObj);
      break;
    case "delete" : 
      debug($reqObj->delArr);
      $sql = "DELETE FROM KINSDB.dbo.tb_rad";
      $sql .= " WHERE cid IN(";

      for($i = 0; $i < count($reqObj->delArr); $i++) {
        $aData = $reqObj->delArr[$i];
        $sql .= $aData;

        if ($i < count($reqObj->delArr) - 1) {
          $sql .= ", ";
        }

        unset($aData);
      }

      $sql .= ")";
      debug($sql);

      $result = sqlsrv_query( $conn, $sql );

      // JSON return
      echo json_encode($rstObj);
      break;
    case "aSelect" : 
      $sql = "SELECT * FROM KINSDB.dbo.tb_rad WHERE cid=".$reqObj->cid;
      $result = sqlsrv_query( $conn, $sql );
      
      $listArr = array();
      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      $rstObj->data["info"] = $listArr[0];

      // JSON return
      echo json_encode($rstObj);
      break;
    case "update" : 
      $dateArr = explode("-", $reqObj->date);
      $date = new DateTime();
      $date->setDate($dateArr[0], $dateArr[1], $dateArr[2]);
      $date->setTime(0, 0);

      $sql = "UPDATE KINSDB.dbo.tb_rad";
      $sql .= " SET";
      $sql .= " country = N'".strip_tags($reqObj->country)."',";
      $sql .= " accident_loc = N'".strip_tags($reqObj->loc)."',";
      $sql .= " accident_facility = N'".strip_tags($reqObj->facility)."',";
      $sql .= " accident_date = '".$date->format("Y-m-d H:i:s")."',";
      $sql .= " accident_level = N'".strip_tags($reqObj->level)."',";
      $sql .= " accident_kins_level = N'".strip_tags($reqObj->kinsLevel)."',";
      $sql .= " update_date = '".date('Y-m-d H:i:s')."'";
      $sql .=" WHERE cid=".$reqObj->cid;

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);

      break;
    case "insert" :
      $dateArr = explode("-", $reqObj->date);
      $date = new DateTime();
      $date->setDate($dateArr[0], $dateArr[1], $dateArr[2]);
      $date->setTime(0, 0);

      $sql = "INSERT INTO KINSDB.dbo.tb_rad (uid, country, accident_loc, accident_facility, accident_date, accident_level, accident_kins_level) ";
      $sql .= " VALUES(";
      $sql .= " ".$reqObj->uid.",";
      $sql .= " N'".strip_tags($reqObj->country)."',";
      $sql .= " N'".strip_tags($reqObj->loc)."',";
      $sql .= " N'".strip_tags($reqObj->facility)."',";
      $sql .= " '".$date->format("Y-m-d H:i:s")."',";
      $sql .= " N'".strip_tags($reqObj->level)."',";
      $sql .= " N'".strip_tags($reqObj->kinsLevel)."'";
      $sql .= ")";

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>