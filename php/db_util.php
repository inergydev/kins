<?php
  include_once ("./define.php");
  include_once ("./logger.php");

  $serverName = DEF_DB_HOST;
  $connectionOptions = array( 
      "Database"=>DEF_DB_NAME, 
      "Uid"=>DEF_DB_USER, 
      "PWD"=>DEF_DB_PASS,
      "CharacterSet" => DEF_DB_CHAR
  );

  function dbConnect() {
    global $serverName;
    global $connectionOptions;
    
    $conn = sqlsrv_connect($serverName, $connectionOptions);
    return $conn;
  }

  function dbDisconnect($conn, $result) {
    sqlsrv_free_stmt( $result );
    sqlsrv_close($conn);
  }

  // Dev
  function dbFormatErrors( $errors )
  {
    debug("Error information: ");  
  
    foreach ( $errors as $error )  
    {  
      debug("SQLSTATE: ".$error['SQLSTATE']);  
      debug("Code: ".$error['code']);  
      debug("Message: ".$error['message']);  
    }
  }

  function dbMssqlEscape($str)
  {
    if(is_numeric($str))
        return $str;
    $unpacked = unpack('H*hex', $str);
    return '0x' . $unpacked['hex'];
  }

  // SQL Injection
  function dbMssqlEscapeString($data) {
    if ( !isset($data) or empty($data) ) return '';
    if ( is_numeric($data) ) return $data;

    $non_displayables = array(
        '/%0[0-8bcef]/',            // url encoded 00-08, 11, 12, 14, 15
        '/%1[0-9a-f]/',             // url encoded 16-31
        '/[\x00-\x08]/',            // 00-08
        '/\x0b/',                   // 11
        '/\x0c/',                   // 12
        '/[\x0e-\x1f]/'             // 14-31
    );
    foreach ( $non_displayables as $regex )
        $data = preg_replace( $regex, '', $data );
    $data = str_replace("'", "''", $data );
    return $data;
  }

?>