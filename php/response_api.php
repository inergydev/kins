<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");
  
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  debug($reqObj);

  // Action
  switch($reqObj->action) {
    case "select" : 
      // Query
      $sql = "SELECT ROW_NUMBER() OVER(ORDER BY regist_date ASC) AS no, * FROM KINSDB.dbo.tb_res";

      $isDateSearch = false;
      if(isset($reqObj->searchOptions))
      {
        $sql .= " WHERE (";
        for ($i = 0; $i < count($reqObj->searchOptions); $i++) {
          $aData = $reqObj->searchOptions[$i];
          $sql .= $aData." LIKE '%".$reqObj->searchKeyword."%'";

          if ($i != count($reqObj->searchOptions) - 1) {
            $sql .= " OR ";
          }
        }
        $sql .= ")";
      }

      debug($sql);

      // Pager Setting
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );

      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
        echo json_encode($rstObj);
        return;
      }

      $pager["totalCount"] = sqlsrv_num_rows($result);
      $pager["currPage"] = (int)$reqObj->page;
      $pager["defPagerPageSize"] = DEF_PAGER_PAGE_SIZE;
      $pager["defPagerBlockSize"] = DEF_PAGER_BLOCK_SIZE;

      // Real Data Select
      $offset = ($reqObj->page * DEF_PAGER_PAGE_SIZE) - DEF_PAGER_PAGE_SIZE;
      $sql .=" ORDER BY regist_date DESC";
      $sql .=" OFFSET ".$offset." ROWS FETCH NEXT ".DEF_PAGER_PAGE_SIZE." ROWS ONLY";
      
      $result = sqlsrv_query( $conn, $sql );
      $listArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      // Data Setting
      $rstObj->data["pager"] = $pager;
      $rstObj->data["listArr"] = $listArr;

      // JSON return
      echo json_encode($rstObj);
      break;
    case "delete" : 
      debug($reqObj->delArr);
      $sql = "DELETE FROM KINSDB.dbo.tb_res";
      $sql .= " WHERE cid IN(";

      for($i = 0; $i < count($reqObj->delArr); $i++) {
        $aData = $reqObj->delArr[$i];
        $sql .= $aData;

        if ($i < count($reqObj->delArr) - 1) {
          $sql .= ", ";
        }

        unset($aData);
      }

      $sql .= ")";
      debug($sql);

      $result = sqlsrv_query( $conn, $sql );

      // JSON return
      echo json_encode($rstObj);
      break;
    case "aSelect" : 
      $sql = "SELECT * FROM KINSDB.dbo.tb_res WHERE cid=".$reqObj->cid;
      $result = sqlsrv_query( $conn, $sql );
      
      $listArr = array();
      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      $rstObj->data["info"] = $listArr[0];

      // JSON return
      echo json_encode($rstObj);
      break;
    case "update" : 
      $sql = "UPDATE KINSDB.dbo.tb_res";
      $sql .= " SET";
      $sql .= " insp_area = N'".strip_tags($reqObj->inspArea)."',";
      $sql .= " insp_air_sea = N'".strip_tags($reqObj->inspAirSea)."',";
      $sql .= " insp_place = N'".strip_tags($reqObj->insPlace)."',";
      $sql .= " contact_name = N'".strip_tags($reqObj->contactName)."',";
      $sql .= " department = N'".strip_tags($reqObj->department)."',";
      $sql .= " monitoring_place = N'".strip_tags($reqObj->monitoringPlace)."',";
      $sql .= " tester_setting_loc = N'".strip_tags($reqObj->testerSettingLoc)."',";
      $sql .= " oper_equip_name = N'".strip_tags($reqObj->operEquipName)."',";
      $sql .= " oper_equip_count = ".$reqObj->operEquipCount.",";
      $sql .= " update_date = '".date('Y-m-d H:i:s')."'";
      $sql .=" WHERE cid=".$reqObj->cid;

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);

      break;
    case "insert" :
      $sql = "INSERT INTO KINSDB.dbo.tb_res (uid, insp_area, insp_air_sea, insp_place, contact_name, department, monitoring_place, tester_setting_loc, oper_equip_name, oper_equip_count) ";
      $sql .= " VALUES(";
      $sql .= " ".$reqObj->uid.",";
      $sql .= " N'".strip_tags($reqObj->inspArea)."',";
      $sql .= " N'".strip_tags($reqObj->inspAirSea)."',";
      $sql .= " N'".strip_tags($reqObj->insPlace)."',";
      $sql .= " N'".strip_tags($reqObj->contactName)."',";
      $sql .= " N'".strip_tags($reqObj->department)."',";
      $sql .= " N'".strip_tags($reqObj->monitoringPlace)."',";
      $sql .= " N'".strip_tags($reqObj->testerSettingLoc)."',";
      $sql .= " N'".strip_tags($reqObj->operEquipName)."',";
      $sql .= " ".$reqObj->operEquipCount;
      $sql .= ")";

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>