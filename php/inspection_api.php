<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");
  
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  debug($reqObj);

  // Action
  switch($reqObj->action) {
    case "select" : 
      // Query
      $sql = "SELECT ROW_NUMBER() OVER(ORDER BY regist_date ASC) AS no, * FROM KINSDB.dbo.tb_ins";

      $isDateSearch = false;
      if(isset($reqObj->searchOptions))
      {
        $sql .= " WHERE (";
        for ($i = 0; $i < count($reqObj->searchOptions); $i++) {
          $aData = $reqObj->searchOptions[$i];
          $sql .= $aData." LIKE '%".$reqObj->searchKeyword."%'";

          if ($i != count($reqObj->searchOptions) - 1) {
            $sql .= " OR ";
          }
        }
        $sql .= ")";
      }

      if(!strcmp($reqObj->searchDateOption, "true")) {
        $dateStartArr = explode("-", $reqObj->dateStart);
        $dateStart = new DateTime();
        $dateStart->setDate($dateStartArr[0], $dateStartArr[1], $dateStartArr[2]);
        $dateStart->setTime(0, 0, 0);

        $dateEndArr = explode("-", $reqObj->dateEnd);
        $dateEnd = new DateTime();
        $dateEnd->setDate($dateEndArr[0], $dateEndArr[1], $dateEndArr[2]);
        $dateEnd->setTime(23, 59, 59);

        if (isset($reqObj->searchOptions)) $sql .= " AND";
        else $sql .= " WHERE";

        $sql .= " insp_date_time BETWEEN '".$dateStart->format("Y-m-d H:i:s")."' AND '".$dateEnd->format("Y-m-d H:i:s")."'";
      }

      debug($sql);

      // Pager Setting
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );

      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
        echo json_encode($rstObj);
        return;
      }

      $pager["totalCount"] = sqlsrv_num_rows($result);
      $pager["currPage"] = (int)$reqObj->page;
      $pager["defPagerPageSize"] = DEF_PAGER_PAGE_SIZE;
      $pager["defPagerBlockSize"] = DEF_PAGER_BLOCK_SIZE;

      // Real Data Select
      $offset = ($reqObj->page * DEF_PAGER_PAGE_SIZE) - DEF_PAGER_PAGE_SIZE;
      $sql .=" ORDER BY regist_date DESC";
      $sql .=" OFFSET ".$offset." ROWS FETCH NEXT ".DEF_PAGER_PAGE_SIZE." ROWS ONLY";
      
      $result = sqlsrv_query( $conn, $sql );
      $listArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      // Data Setting
      $rstObj->data["pager"] = $pager;
      $rstObj->data["listArr"] = $listArr;

      // JSON return
      echo json_encode($rstObj);
      break;
    case "delete" : 
      debug($reqObj->delArr);
      $sql = "DELETE FROM KINSDB.dbo.tb_ins";
      $sql .= " WHERE cid IN(";

      for($i = 0; $i < count($reqObj->delArr); $i++) {
        $aData = $reqObj->delArr[$i];
        $sql .= $aData;

        if ($i < count($reqObj->delArr) - 1) {
          $sql .= ", ";
        }

        unset($aData);
      }

      $sql .= ")";
      debug($sql);

      $result = sqlsrv_query( $conn, $sql );

      // JSON return
      echo json_encode($rstObj);
      break;
    case "aSelect" : 
      $sql = "SELECT * FROM KINSDB.dbo.tb_ins WHERE cid=".$reqObj->cid;
      $result = sqlsrv_query( $conn, $sql );
      
      $listArr = array();
      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $listArr[] = $row;
        }
      }

      $rstObj->data["info"] = $listArr[0];

      // JSON return
      echo json_encode($rstObj);
      break;
    case "update" : 
      $dateArr = explode("-", $reqObj->date);
      $date = new DateTime();
      $date->setDate($dateArr[0], $dateArr[1], $dateArr[2]);
      $date->setTime($dateArr[3], $dateArr[4]);

      $sql = "UPDATE KINSDB.dbo.tb_ins";
      $sql .= " SET";
      $sql .= " insp_date_time = '".$date->format("Y-m-d H:i:s")."',";
      $sql .= " insp_air_sea = N'".strip_tags($reqObj->airSeaType)."',";
      $sql .= " insp_air_sea_desc = N'".strip_tags($reqObj->airSeaDesc)."',";
      $sql .= " register_point = N'".strip_tags($reqObj->registerPoint)."',";
      $sql .= " arrival_num = ".$reqObj->arrivalNum.",";
      $sql .= " passenger_num = ".$reqObj->passengerNum.",";
      $sql .= " insp_tot_num = ".$reqObj->totNum.",";
      $sql .= " update_date = '".date('Y-m-d H:i:s')."'";
      $sql .=" WHERE cid=".$reqObj->cid;

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);

      break;
    case "insert" :
      $dateArr = explode("-", $reqObj->date);
      $date = new DateTime();
      $date->setDate($dateArr[0], $dateArr[1], $dateArr[2]);
      $date->setTime($dateArr[3], $dateArr[4]);

      $sql = "INSERT INTO KINSDB.dbo.tb_ins (uid, insp_date_time, insp_air_sea, insp_air_sea_desc, register_point, arrival_num, passenger_num, insp_tot_num) ";
      $sql .= " VALUES(";
      $sql .= " ".$reqObj->uid.",";
      $sql .= " '".$date->format("Y-m-d H:i:s")."',";
      $sql .= " N'".strip_tags($reqObj->airSeaType)."',";
      $sql .= " N'".strip_tags($reqObj->airSeaDesc)."',";
      $sql .= " N'".strip_tags($reqObj->registerPoint)."',";
      $sql .= " ".$reqObj->arrivalNum.",";
      $sql .= " ".$reqObj->passengerNum.",";
      $sql .= " ".$reqObj->totNum;
      $sql .= ")";

      debug($sql);

      $result = sqlsrv_query( $conn, $sql );
      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
      }
      echo json_encode($rstObj);
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>