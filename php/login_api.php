<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");

  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  // Query
  $sql = "SELECT * FROM KINSDB.dbo.tb_user";
  $sql .= " WHERE id='".strip_tags($reqObj->id)."' AND pwd='".strip_tags($reqObj->pwd)."' AND use_yn = 'Y'";
  
  $result = sqlsrv_query( $conn, $sql );
  $userInfoObj = array();
  
  if ($result != null) {
    while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
      $aData = new stdClass();
      $aData->org_name = $row['org_name'];
      $aData->contact_name = $row['contact_name'];
      $aData->rank = $row['rank'];
      $aData->user_auth = $row['user_auth'];
      $aData->uid = $row['uid'];
      $userInfoObj[] = $aData;
      unset($aData);
    }
  }

  // Data Setting
  $rstObj->userInfoObj = $userInfoObj[0];

  debug($rstObj);

  session_start([
    'cookie_lifetime' => DEF_SESSION_TIMEOUT
  ]);

  $sessionInfo["SESSIONID"] = session_id();
  $sessionInfo["ORG_NAME"] = $rstObj->userInfoObj->org_name;
  $sessionInfo["CONTACT_NAME"] = $rstObj->userInfoObj->contact_name;
  $sessionInfo["RANK"] = $rstObj->userInfoObj->rank;
  $sessionInfo["USER_AUTH"] = $rstObj->userInfoObj->user_auth;
  $sessionInfo["UID"] = $rstObj->userInfoObj->uid;
  
  $_SESSION["INFO"] = $sessionInfo;

  // Exception Process
  if(count($userInfoObj) === 0) {
    $rstObj->errCode = 2;
    $rstObj->errMsg = "Login Faild.";
  } else {
    // Latest Timestamp
    $sql = "UPDATE KINSDB.dbo.tb_user";
    $sql .= " SET last_access_date = '".date('Y-m-d H:i:s')."'";
    $sql .= " WHERE id='".$reqObj->id."' AND uid=".$rstObj->userInfoObj->uid;
    $result = sqlsrv_query( $conn, $sql );
    debug($sql);
    debug($result);
  }

  // JSON return
  echo json_encode($rstObj);

  // DB Disconnection
  dbDisconnect($conn, $result);
?>