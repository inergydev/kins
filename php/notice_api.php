<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");

  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  // Action
  switch($reqObj->action) {
    case "select" : 
      // Query
      $sql = "SELECT ROW_NUMBER() OVER(ORDER BY regist_date ASC) AS no, * FROM KINSDB.dbo.tb_noti";

      if(isset($reqObj->searchOptions))
      {
        $sql .= " WHERE (";
        for ($i = 0; $i < count($reqObj->searchOptions); $i++) {
          $aData = $reqObj->searchOptions[$i];
          $sql .= $aData." LIKE '%".$reqObj->searchKeyword."%'";

          if ($i != count($reqObj->searchOptions) - 1) {
            $sql .= " OR ";
          }
        }
        $sql .= ")";
      }

      debug($sql);

      // Pager Setting
      $params = array();
      $options = array( "Scrollable" => SQLSRV_CURSOR_KEYSET );

      $result = sqlsrv_query( $conn, $sql, $params, $options );

      if ($result == false) {
        dbFormatErrors(sqlsrv_errors());
        $rstObj->errCode = 99;
        $rstObj->errMsg = "DB Precess Error";
        echo json_encode($rstObj);
        return;
      }

      $pager["totalCount"] = sqlsrv_num_rows($result);
      $pager["currPage"] = (int)$reqObj->page;
      $pager["defPagerPageSize"] = DEF_PAGER_PAGE_SIZE;
      $pager["defPagerBlockSize"] = DEF_PAGER_BLOCK_SIZE;

      // Real Data Select
      $offset = ($reqObj->page * DEF_PAGER_PAGE_SIZE) - DEF_PAGER_PAGE_SIZE;
      $sql .=" ORDER BY regist_date DESC";
      $sql .=" OFFSET ".$offset." ROWS FETCH NEXT ".DEF_PAGER_PAGE_SIZE." ROWS ONLY";
      
      $result = sqlsrv_query( $conn, $sql );
      $listArr = array();

      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $aData = new stdClass();
          $aData->no = $row['no'];
          $aData->cid = $row['cid'];
          $aData->uid = $row['uid'];
          $aData->title = urldecode($row['title']);
          $aData->contents = urldecode($row['contents']);
          $aData->attachPath = $row['attach_r_name'];
          $aData->attachRealPath = DEF_FILE_NOTICE_DOWNLOAD_PATH.$row['attach_v_name'];
          $aData->registDate = $row['regist_date'];
          $aData->updateDate = $row['update_date'];
          $listArr[] = $aData;
          unset($aData);
        }
      }

      // Data Setting
      $rstObj->data["pager"] = $pager;
      $rstObj->data["listArr"] = $listArr;

      // JSON return
      echo json_encode($rstObj);

      break;
    case "delete" : 
      debug($reqObj->delArr);
      $sql = "DELETE FROM KINSDB.dbo.tb_noti";
      $sql .= " WHERE cid IN(";

      for($i = 0; $i < count($reqObj->delArr); $i++) {
        $aData = $reqObj->delArr[$i];
        $sql .= $aData;

        if ($i < count($reqObj->delArr) - 1) {
          $sql .= ", ";
        }

        unset($aData);
      }

      $sql .= ")";
      debug($sql);

      $result = sqlsrv_query( $conn, $sql );

      // JSON return
      echo json_encode($rstObj);
      break;
    case "aSelect" :
      $sql = "SELECT * FROM KINSDB.dbo.tb_noti WHERE cid=".$reqObj->cid;
      $result = sqlsrv_query( $conn, $sql );
      
      $listArr = array();
      if ($result != null) {
        while ( $row = sqlsrv_fetch_array( $result, SQLSRV_FETCH_ASSOC )) {
          $row["attachPath"] = $row['attach_r_name'];
          $row["attachRealPath"] = DEF_FILE_NOTICE_DOWNLOAD_PATH.$row['attach_v_name'];
          $row["contents"] = urldecode($row["contents"]);
          $row["title"] = urldecode($row["title"]);
          $listArr[] = $row;
        }
      }

      $rstObj->data["info"] = $listArr[0];

      // JSON return
      echo json_encode($rstObj);
      break;
    case "update" :
      debug($reqObj);
      debug($_FILES);

      if (!strcmp($reqObj->attachState, "none")) {
        $sql = "UPDATE KINSDB.dbo.tb_noti";
        $sql .= " SET";
        $sql .=" title = N'".strip_tags($reqObj->title)."',";
        $sql .=" contents = N'".strip_tags($reqObj->content)."',";
        $sql .=" update_date = '".date('Y-m-d H:i:s')."'";
        $sql .=" WHERE cid=".$reqObj->cid;

        $result = sqlsrv_query( $conn, $sql );
        if ($result == false) {
          dbFormatErrors(sqlsrv_errors());
          $rstObj->errCode = 99;
          $rstObj->errMsg = "DB Precess Error";
        }
      } else if (!strcmp($reqObj->attachState, "insert") || !strcmp($reqObj->attachState, "update")) {
        if ($_FILES["file"]["error"] == UPLOAD_ERR_OK) {
          $fileRealName = $_FILES["file"]["name"];
          $fileVirtualName = "";
          $filePathInfo = pathinfo($fileRealName);
          $fileExtension = $filePathInfo["extension"];
          $fileVirtualName = $reqObj->uid."_".date("YmdHis").".".$fileExtension;
        
          $tmpFilePath = $_FILES['file']['tmp_name'];
          if (move_uploaded_file($tmpFilePath, DEF_FILE_NOTICE_UPLOAD_PATH.$fileVirtualName)) {
            $sql = "UPDATE KINSDB.dbo.tb_noti";
            $sql .= " SET";
            $sql .=" title = N'".strip_tags($reqObj->title)."',";
            $sql .=" contents = N'".strip_tags($reqObj->content)."',";
            $sql .=" attach_r_name = N'".$fileRealName."',";
            $sql .=" attach_v_name = N'".$fileVirtualName."',";
            $sql .=" update_date = '".date('Y-m-d H:i:s')."'";
            $sql .=" WHERE cid=".$reqObj->cid;

            debug($sql);

            $result = sqlsrv_query( $conn, $sql );
            if ($result == false) {
              dbFormatErrors(sqlsrv_errors());
              $rstObj->errCode = 99;
              $rstObj->errMsg = "DB Precess Error";
            }
          } else {
            $rstObj->errCode = 2;
            $rstObj->errMsg = "File Upload Filed";
          }
        } else {
          $rstObj->errCode = 2;
          $rstObj->errMsg = "File Upload Filed";
        }
      } else if (!strcmp($reqObj->attachState, "delete")) {
        $sql = "UPDATE KINSDB.dbo.tb_noti";
        $sql .= " SET";
        $sql .=" title = N'".strip_tags($reqObj->title)."',";
        $sql .=" contents = N'".strip_tags($reqObj->content)."',";
        $sql .=" attach_r_name = '',";
        $sql .=" update_date = '".date('Y-m-d H:i:s')."'";
        $sql .=" WHERE cid=".$reqObj->cid;

        $result = sqlsrv_query( $conn, $sql );
        if ($result == false) {
          dbFormatErrors(sqlsrv_errors());
          $rstObj->errCode = 99;
          $rstObj->errMsg = "DB Precess Error";
        }
      }

      // JSON return
      echo json_encode($rstObj);
      break;
    case "insert" :
      debug($reqObj);
      debug($_FILES);

      if (isset($_FILES["file"])) {
        if ($_FILES["file"]["error"] == UPLOAD_ERR_OK) {
          $fileRealName = $_FILES["file"]["name"];
          $fileVirtualName = "";
          $filePathInfo = pathinfo($fileRealName);
          $fileExtension = $filePathInfo["extension"];
          $fileVirtualName = $reqObj->uid."_".date("YmdHis").".".$fileExtension;
        
          $tmpFilePath = $_FILES['file']['tmp_name'];
          if (move_uploaded_file($tmpFilePath, DEF_FILE_NOTICE_UPLOAD_PATH.$fileVirtualName)) {
            $sql = "INSERT INTO KINSDB.dbo.tb_noti (uid, title, contents, attach_r_name, attach_v_name) ";
            $sql .= " VALUES(";
            $sql .= " '".$reqObj->uid."',";
            $sql .= " N'".strip_tags($reqObj->title)."',";
            $sql .= " N'".strip_tags($reqObj->content)."',";
            $sql .= " N'".$fileRealName."',";
            $sql .= " N'".$fileVirtualName."'";
            $sql .= ")";

            debug($sql);

            $result = sqlsrv_query( $conn, $sql );
            if ($result == false) {
              dbFormatErrors(sqlsrv_errors());
              $rstObj->errCode = 99;
              $rstObj->errMsg = "DB Precess Error";
            }
          } else {
            $rstObj->errCode = 2;
            $rstObj->errMsg = "File Upload Filed";
          }
        } else {
          $rstObj->errCode = 2;
          $rstObj->errMsg = "File Upload Filed";
        }
      } else {
        $sql = "INSERT INTO KINSDB.dbo.tb_noti (uid, title, contents) ";
        $sql .= " VALUES(";
        $sql .= " '".$reqObj->uid."',";
        $sql .= " '".strip_tags($reqObj->title)."',";
        $sql .= " '".strip_tags($reqObj->content)."'";
        $sql .= ")";

        debug($sql);

        $result = sqlsrv_query( $conn, $sql );
        if ($result == false) {
          dbFormatErrors(sqlsrv_errors());
          $rstObj->errCode = 99;
          $rstObj->errMsg = "DB Precess Error";
        }
      }

      // JSON return
      echo json_encode($rstObj);
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>