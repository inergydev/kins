<?php
  include_once ("./logger.php");
  include_once ("./define.php");
  include_once ("./db_util.php");
  include_once ("./session_util.php");
  header("Content-Type:application/json");

  // Request Object
  $reqObj = new stdClass();
  if ( $_POST ) {
    foreach ( $_POST as $key => $value ) {
      $reqObj->$key = $value;
    }
  }

  // Return Object
  $rstObj = new stdClass();
  $rstObj->reqObj = $reqObj;
  $rstObj->errCode = 0;
  $rstObj->errMsg = "success";

  // DB Connection
  $conn = dbConnect();
  if( $conn === false ) {
    $rstObj->errCode = 1;
    $rstObj->errMsg = "DB Connection Faild.";
    echo json_encode($rstObj);
    return;
  }

  // Action
  switch($reqObj->action) {
    case "select" : 
      break;
    case "delete" : 
      break;
    case "aSelect" : 
      break;
    case "update" : 
      break;
    case "insert" : 
      break;
    default:
      break;
  }
  
  // DB Disconnection
  dbDisconnect($conn, $result);
?>